import db from './services/mongoose';
import app from './services/express';
import config from './config';
import cors from "cors";
import passwordResetRoutes from "./routes/passwordResetRouter";

// connect to database then start app
db.connect().then(() => {
  app.use(cors());
  app.listen(config.port, (err) => {
    if (err) {
      console.log(`Error : ${err}`);
      process.exit(-1);
    }
    console.log(`Server çalışıyor ${config.port}`);
  });
});
