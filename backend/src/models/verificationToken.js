import { Schema, model, mongoose } from 'mongoose';
import bcrypt from 'bcrypt';
import httpStatus from 'http-status';
import APIError from '../utils/APIError';

const verificationTokenSchema = new Schema({
    owner: {
        type: Object,
        ref: 'User',
        required: true,
    },
    token: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        expires: 3600,
        default: Date.now()
    }
},
    {
        timestamps: true
    });

verificationTokenSchema.pre('save', async function save(next) {
    if(this.isModified("token")){
        const hash = await bcrypt.hash(this.token, 8);
        this.token=hash; 
    }
    next();
});

verificationTokenSchema.methods.compareToken= async function (token){
    const result = await bcrypt.compareSync(token, this.token);
    return result;
};

export default model('VerificationToken', verificationTokenSchema);