import { Schema, model } from "mongoose";

const newCities = new Schema({
    id: {
        type: Number,
        required: true
    },
    countryId: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    }
})

export default model("Cities",newCities);