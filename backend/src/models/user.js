import { Schema, model } from 'mongoose';
import bcrypt from 'bcrypt-nodejs';
import httpStatus from 'http-status';
import APIError from '../utils/APIError';

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true
  },
  password: {
    type: String,
    required: true,
    minlength: 4,
    maxlength: 24
  },
  name: {
    type: String,
    required: true,
    maxlength: 24
  },
  surName: {
    type: String,
    required: true,
    maxlength: 24
  },
  role: {
    type: String,
    enum: ['user', 'admin'],
    default: 'user'
  },
  verified: { 
    type: Boolean, 
    default: false 
  },
  token:{
    type:String,
    required:false
  },
  media: {
    type: Object,
    required: false
}
},
  {
    timestamps: true
  });

userSchema.pre('save', async function save(next) {
  try {
    if (!this.isModified('password')) {
      return next();
    }
    this.password = bcrypt.hashSync(this.password);
    return next();
  } catch (error) {
    return next(error);
  }
});

userSchema.method({
  transform() {
    const transformed = {};
    const fields = ['id', 'name', 'email',];
    fields.forEach((field) => {
      transformed[field] = this[field];
    });
    return transformed;
  },
  passwordMatches(password) {
    return bcrypt.compareSync(password, this.password);
  }
});

userSchema.statics = {
  checkDuplicateEmailError(err) {
    if (err.code === 11000) {
      var error = new Error('Kullanılmış e-posta');
      error.errors = [{
        field: 'email',
        location: 'body',
        messages: ['Kullanılmış e-posta']
      }];
      error.status = httpStatus.CONFLICT;
      return error;
    }
    return err;
  },

  async findAndGenerateToken(payload) {
    const { email, password } = payload;
    if (!email) throw new APIError('E-posta giriniz!');
    const user = await this.findOne({ email }).exec();
    if (!user) throw new APIError(`E-posta ve kullanıcı adı uyuşmuyor  ${email}`, httpStatus.NOT_FOUND);
    const passwordOK = await user.passwordMatches(password);
    if (!passwordOK) throw new APIError(`Kullanıcı adı ve şifre uyuşmuyor`, httpStatus.UNAUTHORIZED);
    return user;
  }
};

export default model('User', userSchema);
