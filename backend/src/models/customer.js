import { Schema, model } from 'mongoose';
const newCustomer = new Schema({
    user:{
        type:Object,
        required:true
    },
    departure: {
        type: Object,
        minlength: 3,
        maxlength: 15,
        required: true
    },
    arrival: {
        type: Object,
        minlength: 3,
        maxlength: 15,
        required: false
    },
    arrivalDate: {
        type: String,
        minlength: 10,
        maxlength: 250,
        required: true
    },
    departureDate: {
        type: String,
        minlength: 10,
        maxlength: 150,
        required: false
    },
    loadType: {
        type: String,
        minlength: 0,
        maxlength: 30,
        required: false
    },
    loadWeight: {
        type: Number,
        minlength: 0,
        maxlength: 20,
        required: true
    },
    loadSize: {
        type: Number,
        minlength: 0,
        maxlength: 30,
        required: false
    },
    price: {
        type: Number,
        required: true
    },
    descriptions:{
        type: String,
        minlength:10,
        maxlength:1000,
        required:true,
    },
    tab:{
        type: String,
        required:true,
    },
    status: {
        type: Boolean,
        required: true
    },
   /*  media:{
        type:Object,
        required:false
      }, */
      createdAt:Date
});
export default model("Custs", newCustomer);