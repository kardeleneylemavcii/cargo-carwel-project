import { model } from "mongoose";
import { Schema } from "mongoose";
 
const tokenSchema= new Schema({
    userId:{
        type: String,
        required: true,
        ref: 'User',
        unique:true,
    },
    token:{
        type:String,
        required:true,
    },
    createdAt:{
        type:Date,
        default:Date.now,
        expires:3600,
    },
},
    {
        timestamps:true
    }
);

export default model("token", tokenSchema);