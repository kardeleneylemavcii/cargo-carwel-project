import {Schema, model} from 'mongoose';

const favoriteSchema = new Schema({
    userId: {
      type: String,
      required:false,
    },
    postId: {
      type: String,
      required: false
    },
    status: {
      type: Boolean,
      required: false,
      default:true
    },
     favoriteDate:{
        type:Date,
     },
  },
  {
    timestamps: true
  });

export default model('Favorite', favoriteSchema);