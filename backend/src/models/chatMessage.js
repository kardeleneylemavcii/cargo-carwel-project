import {Schema, model} from 'mongoose';

const chatMessageSchema = new Schema({
    message: {
      type: String,
      required: true,
    },
    users: Array,
    senderId: {
      type: String,
      required: true
    },
    roomId: {
      type: String,
      required: true
    },
     seen: { 
       type: Boolean, 
      default: false 
    },
  },
  {
    timestamps: true
  });

export default model('ChatMessage', chatMessageSchema);