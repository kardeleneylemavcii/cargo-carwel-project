import { Schema, model } from "mongoose";

const newCountries = new Schema({
    id: {
        type: Number,
        required: false
    },
    name: {
        type: String,
        required: false,
    },
    shortCode: {
        type: String,
        required: false,
    },
    countryCode:{
        type:Number,
        required:false,
    }
});

export default model ("Countries",newCountries);