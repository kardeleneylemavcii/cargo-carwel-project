import { Schema, model } from 'mongoose';

const mediaSchema = new Schema({
    navbar:{
        logo:{
            title:{
                type:String,
                required:false
            },
             media: {
                type: Object,
                required: false
            }
        }
    },
    homePage:{
        title:{
            type:String,
            required:false
        }, 
         description:{
            type:String,
            required:false
        }, 
        backgroundImg:{
            title:{
                type:String,
                required:false
            },
             media: {
                type: Object,
                required: false
            }
        },
        slider:{
            title:{
                type:String,
                required:false
            }, 
             description:{
                type:String,
                required:false
            },
             media: {
                type: Object,
                required: false
            }
        }
    },
     postPage:{
        title:{
            type:String,
            required:false
        }, 
         description:{
            type:String,
            required:false
        }, 
        backgroundImg:{
            title:{
                type:String,
                required:false
            },
             media: {
                type: Object,
                required: false
            }
        },
        card1:{
            title:{
                type:String,
                required:false
            }, 
             description:{
                type:String,
                required:false
            },
             media: {
                type: Object,
                required: false
            }
        },
        card2:{
            title:{
                type:String,
                required:false
            }, 
             description:{
                type:String,
                required:false
            },
             media: {
                type: Object,
                required: false
            }
        }
    },
    aboutPage:{
        title:{
            type:String,
            required:false
        }, 
         description:{
            type:String,
            required:false
        }, 
        backgroundImg:{
            title:{
                type:String,
                required:false
            },
             media: {
                type: Object,
                required: false
            }
        }
    },
    contactData:{
        title:{
            type:String,
            required:false
        }, 
         description:{
            type:String,
            required:false
        },
        email:{
            type:String,
            required:false
        },
        phone:{
            type:Number,
            required:false
        },
        address:{
            type:String,
            required:false
        },
        watsappURL:{
            type:String,
            required:false
        },
        instagramURL:{
            type:String,
            required:false
        },
        twitterURL:{
            type:String,
            required:false
        },
        facebokURL:{
            type:String,
            required:false
        }       
    },
    getPostPage:{
       title:{
           type:String,
           required:false
       }, 
        description:{
           type:String,
           required:false
       }, 
       backgroundImg:{
           title:{
               type:String,
               required:false
           },
            media: {
               type: Object,
               required: false
           }
       },
   },
   status:{
        type:Boolean,
        required:true
      },
    createdAt: Date
});

export default model("Media", mediaSchema);
