import { Schema, model } from 'mongoose';

const homeMediaSchema = new Schema({
    homeCard:{
        title:{
            type:String,
            required:false
        }, 
        author:{
            type:String,
            required:false
        },
         description:{
            type:String,
            required:false
        }, 
        media: {
                type: Object,
                required: false
            }
    },
 status:{
        type:Boolean,
        required:true
      },
    createdAt: Date,
    updatedAt:Date
});

export default model("HomeMedia", homeMediaSchema);
