import Joi from 'joi';

export const create = {
  body: {
    email: Joi.string().email().required(),
    password: Joi.string().required().label("Password"),
    name: Joi.string().max(128).required(),
  }
};