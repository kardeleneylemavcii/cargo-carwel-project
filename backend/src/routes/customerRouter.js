import express from "express";
import {custsSave,list, custPostDetails,cutsDel,find} from "../controllers/customer";

const router = express.Router();
router.post("/save",custsSave);
router.get("/custsList",list);
router.get("/post/:_id",custPostDetails);
router.delete("/cutsDel/:_id",cutsDel)
router.get("/find/:_id",find);

export default router;