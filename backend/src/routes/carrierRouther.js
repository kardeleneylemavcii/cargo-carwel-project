import express from "express";
import { list ,carrierSave,testController, postDetails,carrierDel,find} from "../controllers/carrier";

const router = express.Router();
router.post("/save",carrierSave);
router.get('/test',testController);
router.get("/carrierList/:departureId?/:arrivalId?",list);
router.get("/post/:_id",postDetails);
router.delete("/carrierDel/:_id",carrierDel)
router.get("/find/:_id",find);

export default router;
