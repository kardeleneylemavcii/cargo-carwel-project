import express from "express";
import authRouter from "./auth";
import carrierRouter from "./carrierRouther";
import customerRouter from "./customerRouter";
import cities from "./cities";
import chatMessageRouter from "./chatMessageRouter";
import homeMediaRouter from "./homeMediaRouter"
import mediaRouter from "./mediaRouter"
import profilRouter from "./profilRouter"
import passwordResetRouter from "./passwordResetRouter";
import adminProfilRouter from "./adminProfilRouter";
import favoriteRouter from "./favoriteRouter";
import postsRouter from "./postsRouter";
import verifyEmailRouter from "./verifyEmailRouter";

const router = express.Router();
router.use("/auth", authRouter);
router.use("/customer", customerRouter);
router.use("/carrier", carrierRouter);
router.use("/cities", cities)
router.use("/chat", chatMessageRouter);
router.use("/profil",profilRouter);
router.use("/homeMedia", homeMediaRouter)
router.use("/media", mediaRouter);
router.use("/passwordReset", passwordResetRouter);
router.use("/verified",verifyEmailRouter);
router.use("/adminProfil",adminProfilRouter);
router.use("/favorite",favoriteRouter);
router.use("/posts",postsRouter);

export default router;
