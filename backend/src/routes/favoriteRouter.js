import express from "express";
import { favoriteSave,favoriteList }from "../controllers/favoriteControl"

const router = express.Router();
router.post("/save",favoriteSave);
router.get("/list/:userId",favoriteList);

export default router;