import express from "express";
import { mediaSave,getAll,mediaRemove,mediaUpdate,getOne} from "../controllers/homeMedia";

const router = express.Router();
router.post('/save',mediaSave);
router.get("/getAll",getAll);
router.get("/getOne/:_id",getOne);
router.post("/remove",mediaRemove);
router.put("/update/:_id",mediaUpdate)

export default router;