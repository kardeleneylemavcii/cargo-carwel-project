import express from "express";
import { resetPassword,getToken,setToken} from "../controllers/password";

const router = express.Router();
router.post('/resetPassword',resetPassword);
router.get('/:id/:token',getToken);
router.post('/:id/:token',setToken);

export default router;