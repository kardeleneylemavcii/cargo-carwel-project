import express from "express";
import { citiesList } from "../controllers/cities";

const router = express.Router();
router.get("/list/:countryId",citiesList);

export default router;