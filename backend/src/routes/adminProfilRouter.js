import express from "express";
import { getAdmin,updateAdmin,updateAdminPass} from "../controllers/adminProfilController";

const router = express.Router();
router.post("/getAdmin",getAdmin);
router.put("/updateAdmin/:_id",updateAdmin);
router.post("/updateAdminPass/:_id",updateAdminPass)

export default router;