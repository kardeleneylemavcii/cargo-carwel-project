import express from "express";
import { getUser,updateUser,updatePass} from "../controllers/profilUser";

const router = express.Router();
router.post("/getUser",getUser);
router.put("/updateUser/:_id",updateUser);
router.post("/updatePass/:_id",updatePass)

export default router;