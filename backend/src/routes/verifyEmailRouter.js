import express from "express";
import { verifiedEmail, getToken, setToken } from "../controllers/verifyAccount";

const router = express.Router();

router.post('/verifiedEmail', verifiedEmail);
router.get('/:id/:token', getToken);
router.post('/:id/:token', setToken);

export default router;