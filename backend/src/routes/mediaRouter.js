import express from "express";
import { fileUpload} from "../controllers/mediaController";

const router = express.Router();
router.post('/upload',fileUpload);

export default router;