import express from "express";
import { all,postDetails} from "../controllers/allPost";

const router = express.Router();
router.post('/all',all);
router.get("/post/:_id",postDetails);

export default router;