import express from 'express';
import validator from 'express-validation';
import { register, login, me, getUser, userDel, verifyEmail, authLogin, withToken } from '../controllers/user';
import { create } from '../validations/user';

const router = express.Router();
//router.post('/register', validator(create), register);
router.post('/register', validator(create), register);
router.get('/login/:email/:password', login);
router.post('/verifyEmail', verifyEmail);
router.get("/getUser", getUser);
router.delete("/userDel/:_id", userDel)
router.get('/authLogin/:email/:password/:role', authLogin);
router.get('/withToken/:token', withToken);

/* router.get('/fakeLogin/:email/:password', fakeLogin); */

export default router;