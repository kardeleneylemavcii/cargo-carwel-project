import express from "express";
import {addMsg, getMsg,getUserMsg,getSeenMsg} from "../controllers/chatMessage";

const router = express.Router();
router.post("/addmsg", addMsg);
router.post("/getMsg", getMsg);
router.get("/getUserMsg/:senderId",getUserMsg)
router.post("/getSeenMsg/:roomId",getSeenMsg)

export default router;