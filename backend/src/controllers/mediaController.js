import { existsSync, mkdirSync, writeFile, chmod } from 'fs';

export const fileUpload = async (req, res, next) => {
  try {
    let encoding = req.body.encoding || 'binary';
    let chroot = 'uploads';
    let file = req.body.outFile;
    let filePath = process.env.INIT_CWD + '/' + chroot + '/' + req.body.path + '/';
    let url = filePath + req.body.fileName;
    // process.env.PWD lınux'ta calışır.process.env.INIT_CWD ise hem lınux hemde windows da çalışır.
    if (!existsSync(process.env.INIT_CWD + '/uploads')) {
      mkdirSync(process.env.INIT_CWD + '/uploads');
    }
    if (!existsSync(filePath)) {
      mkdirSync(filePath);
    }
    chmod(process.env.INIT_CWD + '/uploads', 0o755, (err) => {
      if (err) {
        next(err)
      } else {
        writeFile(url, file, encoding, function (err, filePath) {
          if (err) {
            next(err);
          } else {
            res.send("Ok")
          }
        });
      }
    });
  } catch (error) {
    next(error);
  }
}