import User from "../models/user";
import Token from "../models/token";
import crypto from "crypto";
import Joi from 'joi';
//import { Transporter } from "../utils/mail";
import { mailTransport } from "../utils/mail";
import { generatePasswordEmailTemplate } from "../utils/mail";
import { generatePasswordLoginEmailTemplate } from "../utils/mail";

const ServerEndPoint = "http://localhost:8000/api/";
const BaseURL = "http://localhost:3000/";
export const resetPassword = async (req, res, next) => {
    try {
        const emailSchema = Joi.object({
            email: Joi.string().email().required().label("Email"),
        });
        const { error } = emailSchema.validate(req.body);
        if (error)
            return res.status(400).send({ message: error.details[0].message });
        let user = await User.findOne({ email: req.body.email }).exec();
        if (!user)
            return res.status(409).send({ message: "Mail adresi bulunamadı!" });
        let userId = user._id;
        let token = await Token.findOne({ userId }).exec();
        if (!token) {
            token = await new Token({
                userId: user._id,
                token: crypto.randomBytes(32).toString("hex"),
            }).save();
        }
        const url = `${BaseURL}passwordReset/${user._id}/${token.token}/`;
        if (url && token) {
            /*    let mailOptions={
                   to: req.body.email,
                   subject: "reset password ",
                   text:"bla bla",
                   html: '<div>'+url+'</div>',
               }   
               Transporter.sendMail(mailOptions, function (err,info) {
                   if(err){
                     console.log(err);
                   }
                 });  */
            /* 
                      İsmet Abiye Sor
            console.log(Transporter)
                     Transporter().sendMail({
                        //from: 'security@email.com',
                        to: req.body.email,
                        subject: "reset password ",
                        text:"bla bla",
                        html: '<div>'+url+'</div>',
                    }).then(res=>console.log(res,'<=============then')); 
        
                    //await mail(user.email, "Password Reset", url); */
            mailTransport().sendMail({
                from: 'security@email.com',
                to: user.email,
                text: "Şifre Sıfırlama Bağlantısı",
                html: generatePasswordEmailTemplate(url),
            })
            res.status(200).send({ message: "Şifre Sıfırlama Linki Mail Adresinize Gönderildi!" });
        }
    } catch (error) {
        console.log("error a düştü!!!!!!!!", error);
        res.status(500).send({ message: "Internal Server Error!" });
    }
};

export const getToken = async (req, res) => {
    try {
        const user = await User.findOne({ _id: req.params.id });
        if (!user) return res.status(400).send({ message: "Geçersiz Link!" });
        const token = await Token.findOne({
            userId: user._id,
            token: req.params.token,
        });
        if (!token) return res.status(400).send({ message: "Geçersiz Link" });
        res.status(200).send('Geçerli Link');
    } catch (error) {
        res.status(500).send({ message: "Internal Server Error" });
    }
};

export const setToken = async (req, res) => {
    try {
        /* const passwordSchema = Joi.object({
            password: PasswordComplexity({
                min: 4,
                max: 25,
                lowerCase: 1,
                upperCase: 1,
                numeric: 1,
                requirementCount: 4
            }),
        },
        const { error } = passwordSchema.validate(req.body);
        if (error)
            return res.status(400).send({ message: error.details[0].message }); */
        const user = await User.findOne({ _id: req.params.id });
        if (!user)
            return res.status(400).send({ message: "Geçersiz Link!" });
        const token = await Token.findOne({
            userId: user._id,
            token: req.params.token,
        });
        if (!token) return res.status(400)({ message: "Geçersiz Link" });
        if (!user.verified) user.verified = true;
        /* const salt = await bcrypt.genSalt(Number(process.env.SALT));
        const hashPassword = await bcrypt.hash(req.body.password, salt);
        user.password = hashPassword; */
        user.password = req.body.password;
        await user.save();
        await token.remove();
        mailTransport().sendMail({
            from: 'security@email.com',
            to: user.email,
            text: "Şifre Değiştirme İşlemi Başarılı",
            html: generatePasswordLoginEmailTemplate(),
        });
        res.status(200).send({ message: "Şifre Sıfırlama Başarılı!" });
    } catch (error) {
        res.status(500).send({ message: "Internal Server Error" });
    }
}