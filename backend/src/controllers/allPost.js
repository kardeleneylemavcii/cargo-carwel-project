import httpStatus from "http-status";
import Cuts from "../models/customer";
import Carrier from "../models/carrier";

export const all = async (req, res, next) => {
  try {
    let dataCuts = await Cuts.find({ _id: { $in: req.body } }).exec();
    let dataCarrier = await Carrier.find({ _id: { $in: req.body } }).exec();
    let data = dataCuts.concat(dataCarrier);
    res.send(data, httpStatus.OK)
  } catch (error) {
    return next(error.reason);
  }
};

export const postDetails = async (req, res, next) => {
  try {
    let query = {
      _id: req.params._id
    }
    let postCuts = await Cuts.findOne(query).exec();
    let postCarr = await Carrier.findOne(query).exec();
    if (postCuts !== null) {
      let data = postCuts;
      res.status(httpStatus.CREATED);
      res.send(data)
    } else {
      let data = postCarr;
      res.status(httpStatus.CREATED);
      res.send(data)
    }
  } catch (error) {
    return next(error.reason);
  }
};