import Custs from "../models/customer";
import httpStatus from "http-status";
import { mailTransport } from "../utils/mail";
import { generateCustomerEmailTemplate } from "../utils/mail";
import { generateCustomerRemoveEmailTemplate } from "../utils/mail";

export const custsSave = async (req, res, next) => {
  try {
    console.log(req.body);
    req.body.createdAt = new Date();
    let data = req.body;
    data.status = true;
    data.tab="customer";
    const customers = new Custs(data);
    const savedCustomer = customers.save();
    mailTransport().sendMail({
      from: 'info@email.com',
      to: data.user.email,
      text: "İlanınız yayınlandı!",
      html: generateCustomerEmailTemplate(),
    })
    res.status(httpStatus.CREATED);
    res.send({ savedCustomer });
  } catch (error) {
    return next(error.reason);
  }
};

export const list = async (req, res, next) => {
  try {
    let customerList = await Custs.find().exec();
    res.status(httpStatus.CREATED);
    res.send({ customerList });
  } catch (error) {
    return next(error.reason);
  }
};
export const custPostDetails = async (req, res, next) => {
  try {
    let query = {
      _id: req.params._id,
    }
    let post = await Custs.findOne(query).exec();
   console.log("88888888888888888888888888888888888888",post.user.email);
    res.status(httpStatus.CREATED);
    res.send({ post });
  } catch (error) {
    return next(error.reason);
  }
};
export const cutsDel = async (req, res, next) => {
  try {
    //console.log(req.params, '---------------------');
    let query = {
      _id: req.params._id,
    };
    let post = await Custs.findOne(query).exec();
    mailTransport().sendMail({
      from: 'info@email.com',
      to: post.user.email,
      text: "İlanınız kaldırıldı!",
      html: generateCustomerRemoveEmailTemplate(),
    });
    Custs.remove(query).exec((err, item) => {
      if (err) {
        next(err);
      } else {
        res.send(item, httpStatus.OK);
      }
    });
    

  } catch (err) {
    next(err.reason);
  }
}

export const find = async (req, res, next) => {
  try {

    const data = await Custs.find({"user._id":req.params._id}).exec();

    res.json(data);
  } catch (error) {
    return next(error.reason);
  }
};