import httpStatus from "http-status";
import Favorite from "../models/favorite"

export const favoriteSave = async (req, res, next) => {
  try {
    let data = {
      userId: req.body.userId,
      postId: req.body.postId,
    }
    const deneme = await Favorite.findOne({ userId: data.userId, postId: data.postId }).exec();
    if (deneme !== null && deneme.status) {
      await Favorite.updateOne(data, { $set: { status: false } }).exec();
      res.send(deneme, httpStatus.CREATED);
    }
    if (deneme !== null && !deneme.status) {
      await Favorite.updateOne(data, { $set: { status: true } }).exec();
      res.send(deneme, httpStatus.CREATED);
    }
    if (deneme === null) {
      const favorite = new Favorite(data);
      await favorite.save((err, item) => {
        if (err) {
          next(err);
        } else {
          res.send(item, httpStatus.CREATED);
        }
      })
    }
  } catch (error) {
    return next(error.reason);
  }
};

export const favoriteList = async (req, res, next) => {
  try {
    const data = await Favorite.find(req.params).exec();
    res.json(data);
  } catch (error) {
    return next(error.reason);
  }
};