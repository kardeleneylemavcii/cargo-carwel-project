import Cities from "../models/cities";
import httpStatus from "http-status";

export const citiesList = async (req, res, next) => {
    try {
        let query = {
            countryId: req.params.countryId,         
          };
        let list = await Cities.find(query).exec();
        res.status(httpStatus.CREATED);
        res.send(list);       
    } catch (error) {
        return next(error.reason);
    }
};

/* export const cities = async (req, res, next) => {
    console.log("deneme1");
    try {
        let query = {
            _id: req.params._id
        }
        let post = await Cities.findOne(query).exec();
        res.status(httpStatus.CREATED);
        res.send({ post });
    } catch (error) {
        return next(error.reason);
    }
}; */