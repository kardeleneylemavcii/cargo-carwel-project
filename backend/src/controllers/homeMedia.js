import HomeMedia from "../models/homeMedia";
import httpStatus from "http-status";


export const mediaSave = async (req, res, next) => {
  try {
    req.body.createdAt = new Date();
    let data = {
      homeCard: req.body
    }
    data.status = true;
    const newsRegister = new HomeMedia(data);
    newsRegister.save((err, item) => {
      if (err) {
        next(err);
      } else {
        res.send(item, httpStatus.CREATED);
        console.log("item :::", item)
      }
    })
  } catch (error) {
    return next(error.reason);
  }
};

export const getAll = async (req, res, next) => {
  try {
    let data = await HomeMedia.find().exec();
    res.status(httpStatus.CREATED);
    res.send(data);
  } catch (error) {
    return next(error.reason);
  }
};

export const getOne = async (req, res, next) => {
  try {
    let query = {
      _id: req.params._id
    }
    let data = await HomeMedia.findOne(query).exec();
    res.status(httpStatus.CREATED);
    res.send(data);
  } catch (error) {
    return next(error.reason);
  }
};

export const mediaRemove = async (req, res, next) => {
  try {
    let query = {
      _id: req.body._id
    }
    HomeMedia.remove(query).exec((err, item) => {
      if (err) {
        next(err);
      } else {
        res.send(item, httpStatus.OK);
      }
    });
  } catch (error) {
    next(error)
  }
};

export const mediaUpdate = async (req, res, next) => {
  try {
    let query = {
      _id: req.params._id
    }
    let data = {
      homeCard: req.body
    };
    data.homeCard.updatedAt = new Date;
    HomeMedia.updateMany(query, { $set: data }).exec((err, item) => {
      if (err) { next(err) }
      else { res.send(item) }
    })
  } catch (error) {
    return next(error.reason);
  }
}