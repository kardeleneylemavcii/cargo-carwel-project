import Carrier from "../models/carrier";
import httpStatus from "http-status";
import { mailTransport } from "../utils/mail";
import { generateCarrierEmailTemplate } from "../utils/mail";
import { generateCarrierRemoveEmailTemplate } from "../utils/mail";

export const testController = async (req, res, next) => {
  console.log("testController");
};

export const carrierSave = async (req, res, next) => {
  try {
    req.body.createdAt = new Date();
    let data = req.body;
    data.status = true;
    data.tab = "carrier";
    const carrier = new Carrier(data);
    const savedCarrier = carrier.save();
    mailTransport().sendMail({
      from: 'info@email.com',
      to: data.user.email,
      text: "İlanınız yayınlandı!",
      html: generateCarrierEmailTemplate(),
    })
    res.status(httpStatus.CREATED);
    res.send({ savedCarrier });
  } catch (error) {
    return next(error.reason);
  }
};

export const list = async (req, res, next) => {
  try {
    let query={};
    console.log();
    if(req.params.departureId[0]._id){
      query['departure._id']=req.params.departureId;
    }
    if(req.params.arrivalId){
      query['arrival._id']=req.params.arrivalId;
    }
   console.log(query,' <---- query')
    let carrierList = await Carrier.find(query).exec();
    console.log(carrierList,"carrierList");
    res.status(httpStatus.CREATED);
    res.send({ carrierList });
  } catch (error) {
    return next(error.reason);
  }
};

export const postDetails = async (req, res, next) => {
  try {
    let query = {
      _id: req.params._id
    }
    let post = await Carrier.findOne(query).exec();
    res.status(httpStatus.CREATED);
    res.send({ post });
  } catch (error) {
    return next(error.reason);
  }
};

export const carrierDel = async (req, res, next) => {
  try {
    let query = {
      _id: req.params._id,
    };
    let post = await Carrier.findOne(query).exec();
    mailTransport().sendMail({
      from: 'info@email.com',
      to: post.user.email,
      text: "İlanınız kaldırıldı!",
      html: generateCarrierRemoveEmailTemplate(),
    });
    Carrier.remove(query).exec((err, item) => {
      if (err) {
        next(err);
      } else {
        res.send(item, httpStatus.OK);
      }
    });
  } catch (error) {
    next(error.reason);
  }
};

export const find = async (req, res, next) => {
  try {
    const data = await Carrier.find({ "user._id": req.params._id }).exec();
    res.json(data);
  } catch (error) {
    return next(error.reason);
  }
};