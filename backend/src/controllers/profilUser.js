import httpStatus from 'http-status';
import User from '../models/user';

export const getUser = async (req, res, next) => {
    try {
        let query = {
            _id: req.body._id
        }
        let data = await User.findOne(query).exec();
        res.status(httpStatus.CREATED);
        res.send(data);
    } catch (error) {
        next(error.reason);
    }
};

export const updateUser = async (req, res, next) => {
    try {
        let query = {
            _id: req.params._id
        }
        let data = {
            name: req.body.name,
            surName: req.body.surName,
            // email:req.body.email
        }
        if (req.body.media) {
            data.media = req.body.media
        }
        let updateUser = await User.updateMany(query, { $set: data });
    } catch (error) {
        next(error.reason);
    }
};

export const updatePass = async (req, res, next) => {
    try {
        let query = {
            _id: req.params._id
        }
        const user = await User.findOne(query).exec();
        user.password = req.body.password;
        await user.save();
        res.status(200).send({ message: "Şifre Sıfırlama Başarılı!" });
    } catch (error) {
        next(error.reason);
    }
};