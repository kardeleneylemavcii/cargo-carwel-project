import Media from "../models/media";
import httpStatus from "http-status";
import { existsSync, mkdirSync, writeFile, chmod } from 'fs';

export const mediaSave = async (req, res, next) => {
  try {
    req.body.createdAt = new Date();
    let data = req.body;
    data.status = true;
    const newsRegister = new Media(data);
    newsRegister.save((err, item) => {
      if (err) {
        next(err);
      } else {
        res.send(item, httpStatus.CREATED);
      }
    })
  } catch (error) {
    return next(error.reason);
  }
}
