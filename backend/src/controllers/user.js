import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import config from '../config';
import User from '../models/user';
import VerificationToken from '../models/verificationToken'
import { generateOTP } from '../utils/mail';
import { mailTransport } from '../utils/mail';
import { plainEmailTemplate } from '../utils/mail';
import { generateEmailTemplate } from '../utils/mail';
import APIError from "../utils/APIError";
import bcrypt from "bcrypt-nodejs";
import { Transporter } from "../utils/mail";

export const register = async (req, res, next) => {
    console.log("KAYIT SAYFASI");
    try {
        const beforeUser = new User(req.body);
        const OTP = generateOTP();
        const verificationToken = new VerificationToken({
            owner: beforeUser._id,
            token: OTP,
        });
        /* 
            İsmet abiye sor

        Transporter().sendMail({
            from: 'emailverification@email.com',
            to: beforeUser.email,
            text: "Welcome E-mail ",
            html: generateEmailTemplate(OTP),
        }); */
        mailTransport().sendMail({
            from: "emailverification@email.com",
            to: beforeUser.email,
            text: "E-posta hesabınızı doğrulayın",
            html: generateEmailTemplate(OTP),
        });
        const savedUser = await beforeUser.save();
        const verification = await verificationToken.save();
        res.status(httpStatus.CREATED);
        res.send({
            message: 'Kaydınız alındı,Lütfen Mail hesabınızı onaylayın',
            verification,
            otp: OTP
        });
    } catch (error) {
        return next(User.checkDuplicateEmailError(error));
    }
};

export const verifyEmail = async (req, res, next) => {
    try {
        let query = {
            _id: req.body.owner
        };
        let id = {
            _id: req.body._id
        };
        let otp = req.body.otp;
        let token = await VerificationToken.findOne(id).exec();
        const tokenOK = await token.compareToken(otp);
        if (!tokenOK) throw new APIError(`Kod uyuşmuyor`, httpStatus.UNAUTHORIZED);
        let user = await User.findOne(query).exec();
        if (!user) throw new APIError('Kullanıcı Bulunamadı');
        if (user.verified) throw new APIError(`E-posta zaten onaylı! ${user.verified}`, httpStatus.NOT_FOUND);
        await User.updateOne(query, { $set: { verified: true } });
        let userPost = await User.findOne(query).exec();
        if (!userPost.verified) throw new APIError('Lütfen hesabızını onaylayın');
        await VerificationToken.remove();
        /* 
            İsmet abiye sor
            
        Transporter().sendMail({
            from: 'emailverification@email.com',
            to: beforeUser.email,
            text: "Welcome E-mail ",
            html: plainEmailTemplate(),
        }); */
        mailTransport().sendMail({
            from: 'emailverification@email.com',
            to: userPost.email,
            text: "Welcome E-mail ",
            html: plainEmailTemplate(),
        });
        res.status(200).send({ message: "Email verified successfully" });
    } catch (error) {
        return next(User.checkDuplicateEmailError(error))
    }
};

const passwordMatches = async (password, pass) => {
    return bcrypt.compareSync(password, pass);
};

const findAndGenerateToken = async (payload) => {
    const { email, password } = payload;
    if (!email) throw new APIError('E-posta giriniz!');
    const user = await User.findOne({ email }).exec();
    if (!user.verified) throw new APIError('Lütfen hesabınızı onaylayın');
    if (!user) throw new APIError(`E-posta ve kullanıcı uyuşmuyor ${email}`, httpStatus.NOT_FOUND);
    const passwordOK = await passwordMatches(password, user.password);
    if (!passwordOK) throw new APIError(`Kullancı adı ve şifre uyuşmuyor`, httpStatus.UNAUTHORIZED);
    return user;
};

export const login = async (req, res, next) => {
    try {
        const user = await findAndGenerateToken(req.params);
        const payload = { sub: user.id };
        const token = jwt.sign(payload, config.secret);
        await User.updateOne({_id:user.id},{$set:{token:token}});
        return res.json({
            message: 'OK',
            token,
            user: {
                id: user._id,
                email: user.email,
                name: user.name,
                surName: user.surName
            }
        });
        
    } catch (error) {
        next(error);
    }
};

export const withToken = async (req, res, next) => {
    try {
    const token = req.params.token;
        const user = await User.findOne({token},{email:1,name:1,surname:1,role:1}).exec();
        res.status(httpStatus.CREATED);
        return res.json({
            message: 'OK',
            token,
            user: {
                id: user._id,
                email: user.email,
                name: user.name,
                surName: user.surName
            }
        });
    } catch (error) {
        next(error);
    }
};

export const getUser = async (req, res, next) => {
    try {
        let userList = await User.find().exec();
        res.status(httpStatus.CREATED);
        res.send({ userList });
    } catch (error) {
        return next(error.reason);
    }
};
export const userDel = async (req, res, next) => {
    try {
        let query = {
            _id: req.params._id,
        };
        User.remove(query).exec((err, item) => {
            if (err) {
                next(err);
            } else {
                res.send(item, httpStatus.OK);
            }
        });
    } catch (err) {
        next(err.reason);
    }
};

const findAndAdminGenerateToken = async (payload) => {
    const { email, password, role } = payload;
    if (!email) throw new APIError('E-posta giriniz!');
    const user = await User.findOne({ email }).exec();
    if (!user) throw new APIError(`E-posta ve kullanıcı uyuşmuyor ${email}`, httpStatus.NOT_FOUND);
    const passwordOK = await passwordMatches(password, user.password);
    if (!passwordOK) throw new APIError(`Kullancı adı ve şifre uyuşmuyor`, httpStatus.UNAUTHORIZED);
    if (user.role !== role) throw new APIError(`E-posta ve kullanıcı uyuşmuyor ${role}`, httpStatus.NOT_FOUND);
    return user;
};

export const authLogin = async (req, res, next) => {
    try {
        const user = await findAndAdminGenerateToken(req.params);
        const payload = { sub: user.id };
        const token = jwt.sign(payload, config.secret);
        return res.json({
            message: 'OK',
            token,
            user: {
                id: user._id,
                email: user.email,
                name: user.name,
                surName: user.surName,
                role: user.role
            }
        });
    } catch (error) {
        next(error);
    }
};
