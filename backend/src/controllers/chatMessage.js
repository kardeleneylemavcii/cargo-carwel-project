import ChatMessage from "../models/chatMessage";
import httpStatus from "http-status";

export const addMsg = async (req, res, next) => {
  console.log("chatMessageController::addMsg is running");
  try {
    const { users, message, senderId, roomId } = req.body;
    const data = new ChatMessage({
      message,
      users,
      senderId,
      roomId
    });
    const registeredMsg = await data.save();
    if (registeredMsg) return res.json({ msg: "Message added successfully." });
    else return res.json({ msg: "Failed to add message to the database" });
  } catch (ex) {
    next(ex);
  }
};

export const getMsg = async (req, res, next) => {
  try {
    const messages = await ChatMessage.find(req.body).sort({ createdAt: 1 });
    res.json(messages);
  } catch (ex) {
    next(ex);
  }
};

export const getUserMsg = async (req, res, next) => {
  try {
    let senderId = req.params._id
    let data = await ChatMessage.find(senderId).exec();
    let roomId = data.roomId;
    let chat = await ChatMessage.find(roomId).exec();
    res.json(chat);
  } catch (ex) {
    next(ex);
  }
};

export const getSeenMsg = async (req, res, next) => {
  try {
    let chat = await ChatMessage.find(req.params).exec();
    await chat.forEach(function (item) {
      let query = {
        _id: item._id
      };
      let data = Object.assign(item, { seen: true });
      data.save();
    })
  } catch (ex) {
    next(ex);
  }
}
//updateOne(query, { $set: { seen: true } })
