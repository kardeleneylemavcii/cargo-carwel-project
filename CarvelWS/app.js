const WebSocket = require('ws');
const wss = new WebSocket.Server({ port: 3030 });

wss.on('connection', function connection(ws) {
  //console.log(ws)
  ws.on('message', function incoming( data) {
    wss.clients.forEach(function each(client) {
      if (client !== ws && client.readyState === WebSocket.OPEN) {
        console.log('client',data);
        client.send(data);
      }
    });

  });
});