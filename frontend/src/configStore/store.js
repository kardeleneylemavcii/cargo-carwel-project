import { configureStore } from '@reduxjs/toolkit';
import loginReducer from '../features/loginSlice';
import authReducer from '../features/authSlice';
import otpReducer from '../features/otpSlice';
import  chatRoomReducer from '../features/chatRoomSlice';
export const store = configureStore({
  reducer: {
      login:loginReducer,
      admin:authReducer,
      otp: otpReducer,
      chatRoom:chatRoomReducer
  }
})