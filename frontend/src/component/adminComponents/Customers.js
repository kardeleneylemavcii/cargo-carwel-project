import React, { useState,useEffect } from "react";
import Moment from "moment";
import axios from "axios";
import { Paper, Button } from "@mui/material";
import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Container, Grid } from "@mui/material";

import config from "../../config";

function Customers() {
  const [userData, setUserData] = useState(null);
  const [page, setPage] = React.useState(2);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
    useEffect(() => {
   allUser();
       
    }, []);
    const allUser = () => {
         axios.get(`${config.BACKEND_URL}auth/getUser`).then((res) => {
            console.log(res);
       setUserData(res.data.userList)
        }).catch(err => console.log(err))
    }

   const handleDelCLick = (_id) => {
        console.log("delete yapıldı", _id)
        let status = true;
        axios.delete(`${config.BACKEND_URL}auth/userDel/${_id}`).then(res => {
            console.log(res);
            if (res.statusText === "OK") {
                alert("veriler silindi");
                allUser();
            }
        }).catch(err => {
            console.log(err);
        });
    }
  return (
    <Container component="main" maxWidth="xxl">
        <Grid container spacing={2} >
            <Grid item md={12} xs={12}>
<TableContainer component={Paper}  sx={{ marginTop:2 }}>
    <Table sx={{ maxwidth: "12", minWidth: 12 }} aria-label="simple table">
        <TableHead sx={{ backgroundColor: "#827717" }}>
            <TableRow>
            <TableCell aligin="center"><h3>Role</h3></TableCell>
                <TableCell aligin="center"><h3>Name</h3></TableCell>
                <TableCell aligin="center"><h3>Surname</h3 ></TableCell>
                <TableCell aligin="center"><h3>Email</h3></TableCell>
                <TableCell aligin="center"><h3>Kayıt Tarihi </h3></TableCell>
                <TableCell aligin="center"><h3>KAYIT SİL</h3></TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
                  
        {userData && userData                       
                        .map((item, i) => (
                            <TableRow
                                key={i}
                                sx={{ '&:last-child td, &:last-child th': { border:0 } } }  >
                                <TableCell >{item.role}</TableCell>                          
                                <TableCell >{item.name}</TableCell>
                                <TableCell >{item.surName}</TableCell>
                                <TableCell >{item.email}</TableCell>
                                
                                <TableCell >
                                    {Moment(item.createdAt).format("DD-MM-YYYY HH:MM")}    
                                </TableCell>
                                <TableCell >
                                <Button variant="contained" color="secondary" onClick={() => handleDelCLick(item._id)}>Kayıt Sil</Button></TableCell>                            </TableRow>
                        ))
                    }
        </TableBody>
    </Table>
   
</TableContainer>
            </Grid>
             
        </Grid>
   
</Container>
  )
}

export default Customers;