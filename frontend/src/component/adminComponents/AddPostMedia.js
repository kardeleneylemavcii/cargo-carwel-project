import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Button, Grid, Container } from '@mui/material';
import config from '../../config';
import FileTransfer from '../../mylib/FileTransfer';

const ServerFilesBasePath = "http://localhost:8000/";
function AddPostMedia() {
    const [media, setMedia] = useState();
    const [tempMedia, setTempMedia] = useState(null);


    /*  useEffect(() => {
         allNews();
     }, []);
 
 
     const allNews = () => {
 
         axios.get(`${config.BACKEND_URL}`).then(res => {
             console.log(res.data);
             setMedia(res.data);
         }).catch(err => {
             console.log(err);
         });
 
     } */
    const newsSave = (uploadedMedia) => {
        console.log("working")
        let data = {
            title: "card1"
        }

        if (uploadedMedia) {
            data.media = uploadedMedia
        }
        console.log("mediaaaaaa", data.media);
        axios.post(`${config.BACKEND_URL}media/save`, data).then(res => {
            if (res) {
                alert("Tebrikler! Yeni Haber Kaydı Başarılı!");
            }
        }).catch(err => {
            console.log(err);
        });

    }
    const handleNewsSubmit = () => {
        if (media) {
            FileTransfer(media, (filePath) => {
                console.log(filePath, 'end')
                let fileData = {
                    path: filePath,
                    type: media.type,
                    status: true
                }

                newsSave(fileData);

            });
        } else {
            newsSave();


        }

    }
    const selectMedia = (e) => {
        let file = e.target.files[0];
        let baseType = file.type.split('/')[0];
        console.log(baseType, file);
        setTempMedia(URL.createObjectURL(file));
        console.log(URL.createObjectURL(file))
        setMedia(e.target.files[0]);
    }







    return (
        <Container maxWidth="xxl" sx={{ mt: 4, mb: 4 }}>
            <Grid container spacing={2}>
                <Grid item xs={12} md={8} lg={9}>  {
                    tempMedia
                        ?
                        <img src={tempMedia} style={{ width: "100%",height:"100%" }} />: null
                        }
                        </Grid>
                <Grid item xs={12} md={4} lg={3}>
                    {
                        tempMedia ? 
                        <Button
                            variant="contained"
                            component="label"
                            sx={{width:"100%"}}
                            onClick={() => setTempMedia()}
                        >
                            Vazgeç
                        </Button> : <Button
                            variant="contained"
                            component="label"
                            sx={{width:"100%"}}
                        >
                            Select File
                            <input
                           
                                type="file"
                                /* multiple */
                                accept={'image/*, video/*, audio/*'}
                                hidden
                                onChange={(e) => selectMedia(e)}
                            />
                        </Button>
                    }

                    <Button sx={{width:"100%"}} onClick={() => handleNewsSubmit()}>
                        kaydet
                    </Button>

                </Grid>

            </Grid>
        </Container>
    )
}

export default AddPostMedia