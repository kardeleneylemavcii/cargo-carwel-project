import React ,{ useEffect, useState }from 'react'
import { Box, Container, Grid, Typography } from '@mui/material';
import { AccountProfile } from './AccountProfile';
import { AccountProfileDetails } from './AccountProfileDetails';
import { useSelector } from 'react-redux';
import axios from 'axios';
import config from '../../../config';
import FileTransfer from "../../../mylib/FileTransfer";

const Profil = () => {
  const adminData = useSelector((state) => state.admin.data);
  const [data,setData]=useState(null); // verimizi sağlayamazsak null hata almamamızı sağlıyor.
  const [name,setName]=useState(null);
  const [surName,setSurName]=useState(null);
  const [media, setMedia] = useState();
  const [tempMedia, setTempMedia] = useState(null);
  const [open, setOpen] = useState(false);
useEffect(()=>{
  getUser();
},[])


  const getUser=()=>{
    let query ={
      _id:adminData.id
    }

    axios.post(`${config.BACKEND_URL}adminProfil/getAdmin`,query)
    .then(res=>{
      console.log(res);
      setData(res.data)
    })
    .catch(err=>console.log(err))
  }

  const handleUpdate=(uploadedMedia)=>{

     let updateData={
      name:name||data.name,
      surName:surName||data.surName,
    }
    if (uploadedMedia) {
      updateData.media = uploadedMedia ||data.media
  }
    
    console.log(updateData)
  axios.put(`${config.BACKEND_URL}adminProfil/updateAdmin/${data._id}`,updateData)
   .then(res=>{
    console.log(res);
    alert("güncelleme yapıldı!")
    setOpen(false)
    setTempMedia(null)
  })
  .catch(err=>console.log(err))
  }
  const handleNewsSubmit = () => {
    if (media) {
        FileTransfer(media, (filePath) => {
            console.log(filePath, 'end')
            let fileData = {
                path: filePath,
                type: media.type,
                status: true
            }

            handleUpdate(fileData);

        });
    } else {
      handleUpdate();


    }

}
  const selectMedia = (e) => {
    let file = e.target.files[0];
    let baseType = file.type.split('/')[0];
    console.log(baseType, file);
    setTempMedia(URL.createObjectURL(file));
    console.log(URL.createObjectURL(file))
    setMedia(e.target.files[0]);
}
  return(
  <>
    <Box
      component="main"
      sx={{
        flexGrow: 1,
        py: 8
      }}
    >
      <Container maxWidth="lg">
        <Typography
          sx={{ mb: 3 }}
          variant="h4"
        >
          Account
        </Typography>
        <Grid
          container
          spacing={3}
        >
          <Grid
            item
            lg={4}
            md={6}
            xs={12}
          >
            {
              data ? <AccountProfile 
            selectMedia={selectMedia}
            tempMedia={tempMedia}
            setTempMedia={setTempMedia}
            handleNewsSubmit={handleNewsSubmit}
            setOpen={setOpen}
            open={open}
            data={data}
            />:null
            }
          </Grid>
          <Grid
            item
            lg={8}
            md={6}
            xs={12}
          >
              {
             data ? <AccountProfileDetails
              data={data} 
              setName={setName}
              setSurName={setSurName}
              handleUpdate={handleUpdate}
              />:null
           } 
          </Grid>
        </Grid>
      </Container>
    </Box>
  </>
);
    }
export default Profil;