import React from 'react';
import {Container,Grid,Paper} from "@mui/material"
import AddPostMedia from './HomeMedia/HomeMedia';

function AdminHomePage() {
  return (
    <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
    <Grid container spacing={3}>
      {/* Chart */}
      <Grid item xs={12} md={8} lg={9}>
        <Paper
          sx={{
            display: 'flex',
            flexDirection: 'column',
            height: "100%",
          }}
        >
    Burası dashboarda da bır yeri    
        </Paper>
      </Grid>
      {/* Recent Deposits */}
      <Grid item xs={12} md={4} lg={3}>
        <Paper
          sx={{
            display: 'flex',
            flexDirection: 'column',
            height:"100%",
          }}
        >
          Deposits yeri
        </Paper>
      </Grid>
      {/* Recent Orders */}
      <Grid item xs={12}>
        <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
          orders yeri
        </Paper>
      </Grid>
    </Grid>

  </Container>
  )
}

export default AdminHomePage