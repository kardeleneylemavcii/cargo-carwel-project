import React, { useState } from 'react'
import { Outlet, useNavigate } from 'react-router-dom';
import { Button, Divider, Grid,Container } from "@mui/material";

function Posts() {
  const [page, setPage] = useState(false);
  const navigate=useNavigate();
  return (
    <>
    <Container component="main" maxWidth="xl"
    sx={{display:"flex",flex:"row"}}>
 
      <Grid   item md={12} xs={12} sx={{marginTop:2}} >
        <Grid item   md={12} xs={12} sx={{display:"flex",flex:"row",justifyContent:"center"}}> 
          <Button  md={2} xs={2} variant='outlined' onClick={() => navigate("/adminPostCuts")} >
            PostCustomer
          </Button>
          <Button   md={2}  xs={2}variant='outlined' onClick={() => navigate("/adminPostCarrier")}>
            PostCarrier
          </Button>
       

      </Grid>
      <Divider sx={{ my: 1 }}/>

      <Grid item md={8} xs={8}>
       <Outlet/>
      </Grid>
       </Grid>
     </Container>
     </>
  )
}

export default Posts