import * as React from 'react';

import Button from '@mui/material/Button';

import TextField from '@mui/material/TextField';


import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';

import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import axios from 'axios';
import config from '../../config';
import FormLabel from '@mui/material/FormLabel';
import FormControl from '@mui/material/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormHelperText from '@mui/material/FormHelperText';
import Switch from '@mui/material/Switch';




function AdminAccountRegister() {

  const [state, setState] = React.useState({
    admin: false,
  });

  const handleChange = (event) => {
    setState({
      ...state,
      [event.target.name]: event.target.checked,
    });
  };
  console.log(state)

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const register = {
      email: data.get('email'),
      password: data.get('password'),
      name: data.get('firstName'),
      surName: data.get('lastName'),
      verified:true
    };
    if (state.admin === true) {
      register.role = "admin"
    }
    console.log(register)
    axios.post(`${config.BACKEND_URL}auth/register`, register)
      .then(res => {
        console.log(res)
        alert("Admin Kayıt Yapıldı!")
      })
      .catch(err => console.log(err))
  };



  return (

    <Container component="main" maxWidth="xs">

      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >


        <Typography component="h1" variant="h5">
          REGİSTER
        </Typography>
        <FormControl component="fieldset" variant="standard">
          <FormLabel component="legend">Roles Select</FormLabel>
          <FormGroup>
            <FormControlLabel
              control={
                <Switch checked={state.admin} onChange={handleChange} name="admin" />
              }
              label="Admin"
            />

          </FormGroup>

        </FormControl>
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="given-name"
                name="firstName"
                required
                fullWidth
                id="firstName"
                label="First Name"
                autoFocus
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                id="lastName"
                label="Last Name"
                name="lastName"
                autoComplete="family-name"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="new-password"
              />
            </Grid>

          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign Up
          </Button>
          <Grid container justifyContent="flex-end">
            <Grid item>

            </Grid>
          </Grid>
        </Box>
      </Box>

    </Container>


  );
}
export default AdminAccountRegister;