import React from 'react'
import { Button, Grid, TextField, Box } from '@mui/material';


const HomeMediaDialog = ({ tempMedia, selectMedia, setTitle, setAuthor, setDescription, handleNewsSubmit, setTempMedia}) => {


    return (
            <Grid container spacing={2}>
                <Grid item xs={12} md={4} >
                    {
                        tempMedia
                            ?
                            <img src={tempMedia} style={{ width: "400px",marginTop:50,marginLeft:10}} /> : null
                    }
                </Grid>
                <Grid item xs={12} md={8} spacing={10}>
                    <Box component="form" noValidate sx={{ mt: 3 }}>
                        <Grid container spacing={2}>

                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="title"
                                    label="title"
                                    name="title"
                                    onChange={(e) => setTitle(e.target.value)}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="author"
                                    label="author"
                                    type="author"
                                    id="author"
                                    onChange={(e) => setAuthor(e.target.value)}

                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id="outlined-multiline-static"
                                    label="description"
                                    multiline
                                    rows={10}
                                    style={{ width: "100%" }}
                                    onChange={(e) => setDescription(e.target.value)}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Button sx={{ width: "100%" }} onClick={() => handleNewsSubmit()}>
                                    kaydet
                                </Button>
                                {
                                    tempMedia ?
                                        <Button
                                            variant="contained"
                                            component="label"
                                            sx={{ width: "100%" }}
                                            onClick={() => setTempMedia()}
                                        >
                                            Vazgeç
                                        </Button> : <Button
                                            variant="contained"
                                            component="label"
                                            sx={{ width: "100%" }}
                                        >
                                            Select File
                                            <input

                                                type="file"
                                                /* multiple */
                                                accept={'image/*, video/*, audio/*'}
                                                hidden
                                                onChange={(e) => selectMedia(e)}
                                            />
                                        </Button>
                                }
                            </Grid>
                        </Grid>
                    </Box>
                </Grid>
            </Grid>



    )
}

export default HomeMediaDialog;