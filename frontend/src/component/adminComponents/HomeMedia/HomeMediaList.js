import React, { useState } from 'react'
import { Table, TableHead, TableCell, TableRow, TableBody, Button, TableContainer, Paper, Typography } from '@mui/material';
import config from '../../../config';

export const HomeMediaList = ({ handleDialogOpen, homeData,handleRemove,handleUpdate}) => {

    return (
        <TableContainer component={Paper}>
            <Table sx={{ maxwidth: "12", minWidth: 12 }} aria-label="simple table">
                <TableHead sx={{ backgroundColor: "#827717" }}>
                    <TableRow>
                        <TableCell aligin="center">
                            <Typography variant="h6" gutterBottom component="div">
                                Media
                            </Typography>
                        </TableCell>
                        <TableCell>
                            <Typography variant="h6" gutterBottom component="div">
                                Title
                            </Typography>
                        </TableCell>
                        <TableCell aligin="center">
                            <Typography variant="h6" gutterBottom component="div">
                                Author
                            </Typography>
                        </TableCell>
                        <TableCell aligin="center">
                            <Typography variant="h6" gutterBottom component="div">
                                Depcription
                            </Typography>
                        </TableCell>
                        <TableCell aligin="center">
                            <Typography variant="h6" gutterBottom component="div">
                              Güncelle
                            </Typography>
                        </TableCell>
                        <TableCell aligin="center">
                            <Typography variant="h6" gutterBottom component="div">
                              Kayıt Sil
                            </Typography>
                        </TableCell>

                        <TableCell aligin="right">
                            <Button variant="contained" color="secondary" onClick={handleDialogOpen}>Add +</Button>
                        </TableCell>



                    </TableRow>
                </TableHead>
                <TableBody>


                    {homeData ? homeData.map((item,i) => (
                        <TableRow
                            key={i}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                
                            <TableCell ><img style={{width:"300px"}} src={`${config.BACKEND_BASEPATH}${item.homeCard.media.path}`}/></TableCell>
                            <TableCell >{item.homeCard.title}</TableCell>
                            <TableCell >{item.homeCard.author}</TableCell>
                            <TableCell >{item.homeCard.description}</TableCell>
                            <TableCell >
                                <Button variant="contained" color="secondary" onClick={()=>handleUpdate(item._id)}>Güncelle</Button>
                            </TableCell>
                            <TableCell >
                                <Button variant="contained" color="secondary" onClick={()=>handleRemove(item._id)}>Kayıt Sil</Button>
                            </TableCell>
                        </TableRow>
                    ))
                        : null

                    }
                </TableBody>
            </Table>

        </TableContainer>
    )
}



export default HomeMediaList;
