import React from 'react'
import { Button, Grid, TextField, Box } from '@mui/material';
import config from '../../../config';

const HomeMediaUpdateDialog = ({ tempMedia, selectMedia,setTempMedia,handleUpdateSubmit,setNewTitle,setNewAuthor,setNewDescription,dataOne}) => {


    return (
        <Grid container spacing={2}>
        <Grid item xs={12} md={4} >
       
            {
                tempMedia
                    ?
                    <img src={tempMedia} style={{ width: "400px",marginTop:50,marginLeft:10}} /> : <img style={{width:"300px"}} src={`${config.BACKEND_BASEPATH}${dataOne.media.path}`}/> 
            }
        </Grid>
        <Grid item xs={12} md={8} >
            <Box component="form" noValidate sx={{ mt: 3 }}>
                <Grid container spacing={2}>

                    <Grid item xs={12}>
                        <TextField
                            required
                            fullWidth
                            id="title"
                            label="title"
                            name="title"
                            defaultValue={dataOne.title}
                            onChange={(e) => setNewTitle(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            fullWidth
                            name="author"
                            label="author"
                            type="author"
                            id="author"
                            defaultValue={dataOne.author}
                            onChange={(e) => setNewAuthor(e.target.value)}

                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            id="outlined-multiline-static"
                            label="description"
                            multiline
                            rows={10}
                            style={{ width: "100%" }}
                            defaultValue={dataOne.description}
                            onChange={(e) => setNewDescription(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button sx={{ width: "100%" }} onClick={()=>handleUpdateSubmit()}>
                            kaydet
                        </Button>
                        {
                            tempMedia ?
                                <Button
                                    variant="contained"
                                    component="label"
                                    sx={{ width: "100%" }}
                                    onClick={() => setTempMedia()}
                                >
                                    Vazgeç
                                </Button> : <Button
                                    variant="contained"
                                    component="label"
                                    sx={{ width: "100%" }}
                                >
                                    Select File
                                    <input

                                        type="file"
                                        /* multiple */
                                        accept={'image/*, video/*, audio/*'}
                                        hidden
                                        onChange={(e) => selectMedia(e)}
                                    />
                                </Button>
                        }
                    </Grid>
                </Grid>
            </Box>
        </Grid>
    </Grid>



    )
}

export default HomeMediaUpdateDialog;