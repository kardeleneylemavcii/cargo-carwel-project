import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Grid, Container, Button } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import config from '../../../config';
import FileTransfer from '../../../mylib/FileTransfer';
import HomeMediaDialog from './HomeMediaDialog';
import HomeMediaList from './HomeMediaList';
import HomeMediaUpdateDialog from "./HomeMediaUpdateDialog";

function HomeMedia() {
    const [media, setMedia] = useState();
    const [tempMedia, setTempMedia] = useState(null);
    const [title, setTitle] = useState(null);
    const [author, setAuthor] = useState(null);
    const [description, setDescription] = useState(null);
    const [homeData, setHomeData] = useState(null);
    const [open, setOpen] = useState(false);
    const [pageDialog, setPageDialog] = useState(false);
    const [getId, setGetId] = useState(null);
    const [newTitle, setNewTitle] = useState();
    const [newAuthor, setNewAuthor] = useState();
    const [newDescription, setNewDescription] = useState();
    const [dataOne, setDataOne] = useState();
    const handleDialogOpen = () => {
        setPageDialog(false);
        setOpen(true);
    };

    const handleDialogClose = () => {
        setOpen(false);
        setTempMedia(null)
    };
    useEffect(() => {
        allNews();
    }, []);

    const allNews = () => {
        axios.get(`${config.BACKEND_URL}homeMedia/getAll`).then(res => {
            setHomeData(res.data)
        }).catch(err => {
            console.log(err);
        });
    }

    const newsSave = (uploadedMedia) => {
        console.log("working",dataOne)
        let data = {
            title,
            author,
            description
        }
        if (uploadedMedia) {
            data.media = uploadedMedia ||dataOne.media
        }else{
            data.media = dataOne.media
        }
        axios.post(`${config.BACKEND_URL}homeMedia/save`, data).then(res => {
            if (res) {
                alert("Tebrikler! Yeni Haber Kaydı Başarılı!");
                allNews();
            }
        }).catch(err => {
            console.log(err);
        });
    }

    const handleNewsSubmit = () => {
        if (media) {
            FileTransfer(media, (filePath) => {
                console.log(filePath, 'end')
                let fileData = {
                    path: filePath,
                    type: media.type,
                    status: true
                }
                newsSave(fileData);
            });
        } else {
            newsSave();
        }
    }

    const selectMedia = (e) => {
        let file = e.target.files[0];
        let baseType = file.type.split('/')[0];
        console.log(baseType, file);
        setTempMedia(URL.createObjectURL(file));
        console.log(URL.createObjectURL(file))
        setMedia(e.target.files[0]);
    }

    const handleRemove = (_id) => {
        let query = {
            _id: _id
        }
        axios.post(`${config.BACKEND_URL}homeMedia/remove`, query)
            .then(res => {
                console.log(res)
                allNews();
            })
            .catch(err => console.log(err))
    }
    //buradan sonrası update etme......
    const handleUpdate = (_id) => {
        setPageDialog(true);
        setOpen(true);
        let query = {
            _id: _id
        }
        setGetId(query)
    }
    useEffect(() => {
        getNewsDetail();
    }, [getId]);

    const getNewsDetail = () => {
        console.log(getId)
        if (!getId) {
            console.log("Haber Id Oluşmadı");
            return false;
        };
            axios.get(`${config.BACKEND_URL}homeMedia/getOne/${getId._id}`).then(res => {
             console.log(res)
             setDataOne(res.data.homeCard)
            }).catch(err => console.log(err));
    }           

    const newsUpdate = (uploadedMedia) => {

        console.log(getId)
        console.log("2", uploadedMedia);

        console.log("working")

        let data = {
            title: newTitle ||dataOne.title,
            description: newDescription||dataOne.description ,
            author: newAuthor||dataOne.author
        }
        if (uploadedMedia) {
            data.media = uploadedMedia
        }
        console.log("mediaaaaaa", data.media);
        axios.put(`${config.BACKEND_URL}homeMedia/update/${getId._id}`, data).then(res => {
            if (res) {
                alert("güncelleme yapıldı!");
                allNews();
            }
        }).catch(err => {
            console.log(err);
        });

    }

    const handleUpdateSubmit = () => {
        if (media) {
            FileTransfer(media, (filePath) => {
                console.log(filePath, 'end')
                let fileData = {
                    path: filePath,
                    type: media.type,
                    status: true
                }

                newsUpdate(fileData);

            });
        } else {
            newsUpdate();


        }

    }
    return (
        <Container maxWidth="xxl" sx={{ mt: 4, mb: 4 }}>
            <Grid container spacing={1}>

                <HomeMediaList
                    handleDialogOpen={handleDialogOpen}
                    homeData={homeData}
                    handleRemove={handleRemove}
                    handleUpdate={handleUpdate}
                />
                <Dialog
                    maxWidth={"xl"}
                    open={open}
                    onClose={handleDialogClose}
                    aria-labelledby="responsive-dialog-title"
                >
                    {
                        (pageDialog === false) ? <HomeMediaDialog
                            setTitle={setTitle}
                            setDescription={setDescription}
                            setAuthor={setAuthor}
                            selectMedia={selectMedia}
                            tempMedia={tempMedia}
                            setTempMedia={setTempMedia}
                            handleNewsSubmit={handleNewsSubmit}
                        /> 
                        : 
                        dataOne?<HomeMediaUpdateDialog

                            selectMedia={selectMedia}
                            tempMedia={tempMedia}
                            setTempMedia={setTempMedia}
                            handleUpdateSubmit={handleUpdateSubmit}
                            setNewTitle={setNewTitle}
                            setNewAuthor={setNewAuthor}
                            setNewDescription={setNewDescription}
                            dataOne={dataOne}
                        />:null
                    }

                </Dialog>


                <Grid item xs={12} md={12} >
                    <h1> Buraya ikinci tablo gelecek...............
                    </h1>
                </Grid>


            </Grid>
        </Container>
    )
}

export default HomeMedia;