import React, { useState,useEffect } from "react";
import Moment from "moment";
import axios from "axios";
import { Paper, Button } from "@mui/material";
import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody } from "@mui/material";

import config from "../../config";

function PostCustomer() {

  const [customerData,setCustomerData]=useState(null);

    useEffect(() => {
        allCustomerPost();
       
    }, []);
    const allCustomerPost=()=>{
         axios.get(`${config.BACKEND_URL}customer/custsList`).then((res) => {
            console.log(res);
      setCustomerData(res.data.customerList)
        }).catch(err => console.log(err))
    }

   const handleDelCLick=(_id)=>{
        console.log("delete yapıldı",_id)
        let status = true;
        axios.delete(`${config.BACKEND_URL}customer/cutsDel/${_id}`).then(res => {
            console.log(res);
            if (res.statusText === "OK") {
                alert("veriler silindi");
                allCustomerPost();
            }
        }).catch(err => {
            console.log(err);
        });
    }
  return (
    <TableContainer component={Paper}>
    <Table sx={{ maxwidth: "12", minWidth: 12 }} aria-label="simple table">
        <TableHead sx={{ backgroundColor: "#827717" }}>
            <TableRow>
                         <TableCell><h3>Yük Alınacak Yer</h3></TableCell>
                        <TableCell aligin="center"><h3>Yük Teslim Edilecek Yer</h3></TableCell>
                        <TableCell aligin="center"><h3>Gidiş Tarihi</h3 ></TableCell>
                        <TableCell aligin="center"><h3>Varış Tarihi</h3></TableCell>
                        <TableCell aligin="center"><h3>Yük Ağırlığı </h3></TableCell>
                        <TableCell aligin="center"><h3>Yük Büyüklüğü(kg)</h3></TableCell>
                        <TableCell aligin="center"><h3>Yük Türü</h3></TableCell>
                        <TableCell aligin="center"><h3>Kayıt Tarihi</h3></TableCell>

                <TableCell aligin="center"><h3>KAYIT SİL</h3></TableCell>
            </TableRow>
        </TableHead>
        <TableBody>

                  
        {customerData && customerData
                       
                        .map((item, i) => (
                            <TableRow
                                key={i}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}                            >
                               <TableCell >{item.departure.name}</TableCell>
                                <TableCell >{item.arrival.name}</TableCell>
                                <TableCell >{Moment(item.departureDate).format("DD-MM-YYYY HH:MM")}</TableCell>
                                <TableCell >{Moment(item.arrivalDate).format("DD-MM-YYYY HH:MM")}</TableCell>
                                <TableCell >{item.loadWeight ? item.loadWeight : "Bilinmiyor"}</TableCell>
                                <TableCell >{item.loadSize}</TableCell>
                                <TableCell >{item.loadType}</TableCell>
                                <TableCell >
                                    {Moment(item.createdAt).format("DD-MM-YYYY HH:MM")}    
                                </TableCell>
                                <TableCell >
                                    <Button variant="contained" color="secondary" onClick={()=>handleDelCLick(item._id)}>Kayıt Sil</Button></TableCell>
                            </TableRow>
                        ))

                    }
        </TableBody>
    </Table>
 
</TableContainer>
  )
}

export default PostCustomer