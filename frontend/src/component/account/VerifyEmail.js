import React, { useState } from 'react';
import axios from 'axios';
import { useSelector } from 'react-redux';
import PinInput from "react-pin-input";
import styles from "./styles.module.css";

const ServerEndPoint = "http://localhost:8000/api/";

export default function VerifyEmail() {
    const [otp, setOtp] = useState(null);
    const [valid, setValid] = useState(false);
    const user = useSelector(state => state.otp.data);
    const savedUser = (e) => {
        e.preventDefault();
        let data = {
            _id: user.verification._id,
            owner: user.verification.owner,
            otp: otp
        }
        axios.post(ServerEndPoint + "auth/verifyEmail", data)
            .then((res) => {
                alert("Hesabınız Onaylandı!");
                window.location = "/login";
            })
            .catch(error => console.log(error))
    };

    return (
        <div className={styles.container}>
            <form className={styles.form_container}>
                <h1>HESABINIZI DOĞRULAYIN</h1>
                <PinInput
                    length={4}
                    initialValue=""
                    onChange={(e) => setOtp(e)}
                    //secret
                    type="numeric"
                    inputMode="number"
                    inputStyle={{ border: '8px solid', borderColor: 'red' }}
                    inputFocusStyle={{ borderColor: 'blue' }}
                    onKeyPress={(e) => {
                        if (e.key === "Enter") {
                            setOtp(e);
                            savedUser()
                        }
                    }}
                    onComplete={(e) => setOtp(e)}
                />
                <button onClick={(e) => savedUser(e)} className={styles.green_btn}>
                    ONAYLA
                </button>
            </form>
        </div>


    );
}