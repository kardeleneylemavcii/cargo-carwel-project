import React, { useState } from 'react';
import axios from 'axios';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import Link from '@mui/material/Link';
import { setOtp } from '../../features/otpSlice';

const ServerEndPoint = "http://localhost:8000/api/";

export default function AccountDialog({ setDialog }) {
  const [name, setName] = useState(null);
  const [surName, setSurName] = useState(null);
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const postSavedUser = () => {
    let data = {
      name: name,
      surName: surName,
      email: email,
      password: password,
      verification: false
    }
    axios.post(ServerEndPoint + "auth/register", data)
      .then(res => {
        console.log(res);
        dispatch(setOtp(res.data));
        setDialog(false);
        navigate('/verifyEmail')
      }
      )
      .catch(err => console.log(err.response.data))
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Kayıt Olun
        </Typography>
        <Box component="form" noValidate sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="given-name"
                name="firstName"
                required
                fullWidth
                id="firstName"
                label="İsminiz"
                autoFocus
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    setName(e.target.value);
                    postSavedUser();
                  }
                }}
                onChange={(e) => setName(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                fullWidth
                id="lastName"
                label="Soyisminiz"
                name="lastName"
                autoComplete="family-name"
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    setSurName(e.target.value);
                    postSavedUser();
                  }
                }}
                onChange={(e) => setSurName(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="email"
                label="Email Adresi"
                name="email"
                autoComplete="email"
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    setEmail(e.target.value);
                    postSavedUser();
                  }
                }}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="password"
                label="Şifre"
                type="password"
                id="password"
                autoComplete="new-password"
                onKeyPress={(e) => {
                  if (e.key === "Enter") {
                    setPassword(e.target.value);
                    postSavedUser();
                  }
                }}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label="E-posta yoluyla güncelleme ve reklam bildirimlerini almak istiyorum."
              />
            </Grid>
          </Grid>
          <Link to="/verifyEmail">
            <Button
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              onClick={() => postSavedUser()}
            >
              Kayıt Olun
            </Button>
          </Link>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Button onClick={() => setDialog(false)}>
                Hesabınız Mı Var? Giriş Yapın
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  );
};
