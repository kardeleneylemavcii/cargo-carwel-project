import React from "react";
import { useSelector } from "react-redux";
import Moment from "moment";
import {
    Avatar,
    Grid,
  ListItem,
    ListItemAvatar,
    ListItemText,
    Box
  } from "@mui/material";

const ChatMessage = ({ message, senderId, createdAt }) => {
    const userData = useSelector((state) => state.login.data);
    const messageSide = () => {
      let res = "left";
  
      if (senderId === userData.id) {
        res = "right";
      }
      return res;
    }
    const style = () => {
      let msj = {
        textAlign:"left",
        position: "relative",
        marginBottom: "10px",
        padding: "10px",
        backgroundColor: "#A8DDFD",
        width: "100%",
        //height: "50px",
        textAlign: "left",
        font: "400 .9em 'Open Sans', sans-serif",
        border: "1px solid #97C6E3",
        borderRadius: "10px",
        "&:after": {
          content: "''",
          position: "absolute",
          width: "0",
          height: "0",
          borderTop: "15px solid #A8DDFD",
          borderLeft: "15px solid transparent",
          borderRight: "15px solid transparent",
          top: "0",
          left: "-15px"
        },
        "&:before": {
          content: "''",
          position: "absolute",
          width: "0",
          height: "0",
          borderTop: "17px solid #97C6E3",
          borderLeft: "16px solid transparent",
          borderRight: "16px solid transparent",
          top: "-1px",
          left: "-17px"
        }
      } 
      if (senderId === userData.id) {
        msj = {
          textAlign:"right",
          position: "relative",
          marginBottom: "10px",
          padding: "10px",
          backgroundColor: "#f8e896",
          width: "1000%",
          //height: "50px",
          textAlign: "left",
          font: "400 .9em 'Open Sans', sans-serif",
          border: "1px solid #dfd087",
          borderRadius: "10px",
          "&:after": {
            content: "''",
            position: "absolute",
            width: "0",
            height: "0",
            borderTop: "15px solid #f8e896",
            borderLeft: "15px solid transparent",
            borderRight: "15px solid transparent",
            top: "0",
            right: "-15px"
          },
          "&:before": {
            content: "''",
            position: "absolute",
            width: "0",
            height: "0",
            borderTop: "17px solid #dfd087",
            borderLeft: "16px solid transparent",
            borderRight: "16px solid transparent",
            top: "-1px",
            right: "-17px"
          }
        }
      }
      return msj;
    }
    return (
    <ListItem key="1" sx={{p:4}}>
        <Grid container  sx={style()}>
          <Grid item xs={12}>
            <ListItemText primary={message}>{message}</ListItemText>
          </Grid>
          <Grid item xs={12}>
            <ListItemText style={{ textAlign: messageSide() }} secondary={Moment(createdAt).format("DD-MM-YYYY HH:MM")}></ListItemText>
          </Grid>
        </Grid>
      </ListItem>
  
    )
  }
  export default ChatMessage;


