import React, { useState } from 'react'
import ReactBoxFlip from "react-box-flip";
import AddPostCarrier from './AddPostCarrier';
import { Button, Grid, Container } from '@mui/material';
import { ForkRightOutlined } from "@mui/icons-material"
import nakliyat from "../../img/nakliyat.jpg"

const CardFlipPostFormCarrier = () => {
    const [isFlipped, setIsFlipped] = useState(false);
    function handleClick() {
        setIsFlipped(!isFlipped);
    }
    return (
        <Container maxWidth="xl" sx={{ mb: -5, mr: -8 }}>
            <Button sx={{ mt: -9, ml: 8, backgroundColor: "#127d88", color: "#38263b" }} variant="outlined" color="secondary" onClick={handleClick}>
                Nakliyat ve Taşımacılık İlan Kayıt Formu
                <ForkRightOutlined />
            </Button>
            <Grid container spacing={2}>
                <Grid item xs={12} md={3}>

                </Grid>
                <Grid item xs={12} md={10}>
                    <ReactBoxFlip isFlipped={isFlipped} >
                        <div>
                            <img className="pic" style={{ width: 500, height: 550, border: 2, borderRadius: 10, boxShadow: " 10px 10px 10px #000" }} src={nakliyat} alt="Jimmy Eat World" />
                        </div>
                        <div>
                            <AddPostCarrier />
                        </div >
                    </ReactBoxFlip>
                </Grid>
            </Grid>
        </Container>
    );
}

export default CardFlipPostFormCarrier;


