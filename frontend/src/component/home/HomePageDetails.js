import React from "react";
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import tırtır from "../../img/tırtır.jpg";
import tır from "../../img/tır.jpg";
import para from "../../img/para.jpg";
import message from "../../img/mobile-app.png";
import follow from "../../img/follow.png";
import truck from "../../img/truck.png";
import { Container } from "@mui/material";
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { useNavigate } from "react-router-dom";
const item = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    px: 5,
};


const image = {
    height: 55,
    my: 4,
};


function HomePageDetails() {
    const navigate = useNavigate();
    return (
        <div>

            <Typography color="inherit" align="center" variant="h4" marked="center">
                CARWEL'E HOŞGELDİNİZ
            </Typography>
            <Typography
                color="inherit"
                align="center"
                variant="h5"
                sx={{ mb: 4, mt: { sx: 4, sm: 10 } }}
            >
                KOLAY TAŞIMACILIĞIN ADRESİNE HOŞGELDİNİZ!
            </Typography>

            <Box
                component="section"
                sx={{ display: 'flex', overflow: 'hidden' }}
            >


                <Container sx={{ mt: 10, mb: 10, display: 'flex', position: 'relative' }}>

                    <Grid container spacing={5}>
                        <Grid item xs={12} md={4}>
                            <Box sx={item}>
                                <Box
                                    component="img"
                                    src={tırtır}
                                    alt="tır"
                                    sx={{ height: 100, borderRadius: 5 }}
                                />
                                <Typography variant="h6" sx={{
                                    margin: 0,
                                    marginTop: "40px",
                                    marginBottom: "40px",
                                    fontSize: " large",
                                    color: " white",
                                    fontStyle: "bold",
                                    fontWeight: "bold "
                                }}>
                                    KOLAY TAŞIMACILIK
                                </Typography>
                                <Typography variant="h5" sx={{
                                    fontSize: "small",
                                    fontFamily: "auto",
                                    fontWeight: 400,
                                    justifyContent: "center",
                                    textAlign: "center"
                                }}>
                                    {
                                        'İlanınızı verin ya da kargo şirketlerinin ilanlarına bakın'
                                    }
                                    {
                                        ', iletişime geçin ve kolaylığın tadını çıkartın.'
                                    }
                                </Typography>
                            </Box>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <Box sx={item}>
                                <Box
                                    component="img"
                                    src={tır}
                                    alt="tır"
                                    sx={{ height: 100, borderRadius: 5 }}
                                />
                                <Typography variant="h6" sx={{
                                    margin: 0,
                                    marginTop: "40px",
                                    marginBottom: "40px",
                                    fontSize: " large",
                                    color: " white",
                                    fontStyle: "bold",
                                    fontWeight: "bold "
                                }}>
                                    KOLAY MÜŞTERİ
                                </Typography>
                                <Typography variant="h5" sx={{
                                    fontSize: "small",
                                    fontFamily: "auto",
                                    fontWeight: 400,
                                    justifyContent: "center",
                                    textAlign: "center"
                                }}>
                                    {
                                        'Gideceğiniz yere ilan açın ya da müşterilerin ilanlarına göz atın'
                                    }
                                    {
                                        'boş araçla yolculuk yapmayın.'
                                    }
                                </Typography>
                            </Box>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <Box sx={item}>

                                <Box
                                    component="img"
                                    src={para}
                                    alt="kazanın"
                                    sx={{ height: 100, borderRadius: 5 }}
                                />
                                <Typography variant="h6" sx={{
                                    margin: 0,
                                    marginTop: "40px",
                                    marginBottom: "40px",
                                    fontSize: " large",
                                    color: " white",
                                    fontStyle: "bold",
                                    fontWeight: "bold "
                                }}>
                                    BERABER KAZANMA
                                </Typography>
                                <Typography variant="h5" sx={{
                                    fontSize: "small",
                                    fontFamily: "auto",
                                    fontWeight: 400,
                                    justifyContent: "center",
                                    textAlign: "center"
                                }}>
                                    {
                                        'Uygulamayı kullanmaya başlayın ve '
                                    }
                                    {
                                        'hem siz kazanın hem de anlaştığız kişi kazansın.'
                                    }
                                </Typography>
                            </Box>
                        </Grid>
                    </Grid>
                </Container>
            </Box>

            <Grid
                container
                direction="row-reverse"
                justifyContent="center"
                alignItems="center"
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}>

                <Grid item xs={12} md={6}>
                    <Grid container
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                    >

                        <Grid item xs={12} md={6}>
                            <Typography variant="h5" sx={{
                                fontSize: "small",
                                fontFamily: "auto",
                                fontWeight: 400,
                                justifyContent: "center",
                                textAlign: "center"
                            }}>
                                Size uygun ilanı veren kişiyle iletişime geçin ve takipte kalın.
                            </Typography>
                        </Grid>
                        <Grid item xs={12} md={6} sx={{textAlign:"center"}}>
                            <Button
                                color="secondary"
                                size="large"
                                variant="contained"
                                onClick={()=>navigate("post")}
                                sx={{ borderRadius: '3px', border: "1px solid white", background: '#36a7b791' }}
                            >
                                İlanlara Bakın
                                <ArrowForwardIcon fontSize="10" sx={{ marginLeft: 3 }} />
                            </Button>
                        </Grid>


                    </Grid>

                </Grid>
                <Grid item xs={12} md={6}>
                    <Grid container
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                    >

                        <Grid item xs={12} md={6}>
                            <Typography variant="h5" sx={{
                                fontSize: "small",
                                fontFamily: "auto",
                                fontWeight: 400,
                                justifyContent: "center",
                                textAlign: "center"
                            }}>
                                Geriye sadece anlaşmak kaldı. Sürekli yenilenen ilanlarla her zaman hayatınızı kolaylaştırın!
                            </Typography></Grid>
                        <Grid item xs={12} md={6} sx={{textAlign:"center"}}>
                            <Button
                                variant="contained"
                                size="large"
                                color="secondary"
                                onClick={()=>navigate("addPost")}
                                sx={{ borderRadius: '3px', border: "1px solid white", background: '#36a7b791' }}
                            >
                                İlan Verin
                                <ArrowForwardIcon fontSize="10" sx={{ marginLeft: 3 }} />
                            </Button>
                        </Grid>


                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}

export default HomePageDetails