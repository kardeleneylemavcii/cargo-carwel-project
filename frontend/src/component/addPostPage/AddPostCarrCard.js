import React, { useState } from 'react';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import AddPostCarrDialog from './AddPostCarrDialog';
import { Dialog } from '@mui/material';


export default function AddPostCarrCard() {
    const [dialog, setDialog] = useState(false)
    return (
        <>
            <Paper
                onClick={() => setDialog(true)}
                sx={{
                    p: 2,
                    //margin: 'auto',
                    //maxWidth: 500,
                    height: 200,
                    display: "flex",
                    cursor: "pointer",
                    justifyContent: "center",
                    alignItems: "center",
                    //flexGrow: 1,
                    "&:hover": {
                        boxShadow: " 0px 0px 2px 5px #9b8076"
                    },

                    opacity: 0.75,
                    textAlign: "center",
                    backgroundImage: `url(${'https://images.unsplash.com/photo-1616432043562-3671ea2e5242?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80'})`,
                    backgroundColor: (theme) =>
                        theme.palette.mode === 'dark' ? '#1A2027' : '#fff',

                }}
            >
                <Typography variant="h5" sx={{
                    fontSize: "large",
                    fontFamily: "auto",
                    fontWeight: "bold",
                    color: "white"
                    //justifyContent: "center",
                    // textAlign: "center"
                }}>
                    Size uygun ilanı bulamadınız mı?
                    O zaman Araç Taşıma Kapasitenize göre bir değerlendirmeye ve
                    potansiyel nakliyat hizmetleri için yükünüze özel ilan oluşturun!
                </Typography>



            </Paper>
            <Dialog
                open={dialog}
                onClose={() => setDialog(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <AddPostCarrDialog setDialog={setDialog} />
            </Dialog>
        </>
    );
}