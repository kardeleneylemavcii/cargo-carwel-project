import React from 'react'
import TextField from '@mui/material/TextField';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import Stack from '@mui/material/Stack';
import Grid from '@mui/material/Grid';

function AddPostCustomerEditDate({ setDepartureDate, setArrivalDate, departureDate, arrivalDate }) {
  console.log("value:::::", departureDate);
  console.log("value:::::", arrivalDate);

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <DateTimePicker
            renderInput={(params) => <TextField {...params} />}
            label="YÜKÜN TESLİM VERİLECEĞİ TARİH"
            value={departureDate}
            onChange={(newValue) => {
              setDepartureDate(newValue)
            }}
            minDateTime={new Date()}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <DateTimePicker
            renderInput={(params) => <TextField {...params} />}
            label="YÜKÜN TESLİM EDİLECEĞİ TARİH"
            value={arrivalDate}
            onChange={(newValue) => {
              setArrivalDate(newValue);
            }}
            minDateTime={new Date()}
          />
        </Grid>
      </Grid>
    </LocalizationProvider>
  )
}

export default AddPostCustomerEditDate