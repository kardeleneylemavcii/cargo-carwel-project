import React, { useState, useEffect } from "react";
import Button from '@mui/material/Button';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import AddPostCarrierEditForm from './AddPostCarrierEditForm';
import axios from 'axios';
import ws from '../../lib/WsConfig';
import waitForOpenConnection from "../../lib/WsWaitForConn";
import { useSelector } from "react-redux";
const ServerEndPoint = "http://localhost:8000/api/";

export default function AddPostCarrDialog({ setDialog }) {
   

  /*  const [media, setMedia] = useState();
     const [tempMedia, setTempMedia] = useState(null); */
     const [departure, setDeparture] = useState();
     const [arrival, setArrival] = useState();
     const [departureDate, setDepartureDate] = useState();
     const [arrivalDate, setArrivalDate] = useState();
     const [loadWeight, setLoadWeight] = useState();
     const [loadSize, setLoadSize] = useState();
     const [loadType, setLoadType] = useState();
     const [price, setPrice] = useState();
     const [cityList, setCityList] = useState([]);
     const [descriptions, setDescriptions] = useState();
     const userData = useSelector((state) => state.login.data);
     /*  const selectMedia = (e) => {
          let file = e.target.files[0];
          let baseType = file.type.split('/')[0];
          console.log(baseType, file);
          setTempMedia(URL.createObjectURL(file));
          console.log(URL.createObjectURL(file))
          setMedia(e.target.files[0]);
      } */
     useEffect(() => {
         console.log("AddPostCarrier");
         axios.get(ServerEndPoint + "cities/list/223").then(res => {
             setCityList(res.data)
             console.log(res);
         }).catch(err =>
             console.log(err))
     }, []);
 
     const broadcast = (msg, room) => {
         console.log(msg, room, 'broadcast');
         ws.send(JSON.stringify({ msg: msg, room: room }))
     }
     const join = (room, socket) => {
         console.log(room, 'join');
         ws.send(JSON.stringify({ join: room }));
     }
     let socket = ws;
     const group = 'post';
     useEffect(() => {
 
         if (socket.readyState !== socket.OPEN) {
             try {
                 console.log("socket not open");
                 waitForOpenConnection(socket).then(r => {
                     join(group, socket, "if");
                 })
             } catch (err) {
                 console.error(err)
             }
         } else {
             console.log('else', socket);
             join(group, socket);
         }
         socket.onmessage = event => {
             if (event.data instanceof Blob) {
                 const reader = new FileReader();
 
                 reader.onload = () => {
                     console.log("Result: " + reader.result);
                 };
 
                 reader.readAsText(event.data);
             } else {
                 console.log("Result: " + event.data);
             }
             /*    const getEventData = JSON.parse(evt.data);
                const content = getEventData.msg; */
 
             //console.log(content, this.state.user);
             /* this.setState(() => {
                 return {content: content, room:getEventData.room}
             })
             if (!getEventData.room) {
                 return false;
             }
             if (content.userId === this.state.user.id) {
                 this.getNotification();
             } */
         };
     }, []);
 
     const carrierSave = () => {
         broadcast("aloha ve de maloha", group);
         let carrPostData = {
             departureDate,
             arrivalDate,
             departure: departure[0],
             arrival: arrival[0],
             loadWeight,
             loadSize,
             loadType,
             price,
             descriptions,
             user: {
                 _id: userData.id,
                 name: userData.name,
                 email: userData.email,
             }
             /*  media,
              tempMedia, */
         }
         console.log(carrPostData);
         /* if(uploadedMedia) {
            carrPostData.media =uploadedMedia
        } */
         axios.post(ServerEndPoint + "carrier/save", carrPostData).then(res => {
             if (res) {
                 alert("Tebrikler! Yeni İlanınız Başarıyla Kaydoldu!");
             }
         }).catch(err => {
             console.log(err);
         });
     }

  return (
    <div>
    
    <DialogTitle sx={{textAlign:"center",backgroundColor:"#127d88",fontWeight:"bold",color:"white",borderBottomLeftRadius: "30px",borderBottomRightRadius: "30px"}}>Carrier ilan oluşturma</DialogTitle>
        <DialogContent>
        <AddPostCarrierEditForm
                departureDate={departureDate}
                arrivalDate={arrivalDate}
                setDeparture={setDeparture}
                setArrival={setArrival}
                setDepartureDate={setDepartureDate}
                setArrivalDate={setArrivalDate}
                setLoadWeight={setLoadWeight}
                setLoadSize={setLoadSize}
                setLoadType={setLoadType}
                setPrice={setPrice}
                cityList={cityList}
                setDescriptions={setDescriptions}
                /* selectMedia={selectMedia}
                tempMedia={tempMedia}
                setTempMedia={setTempMedia} */ />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setDialog(false)}>Cancel</Button>
          <Button onClick={() => carrierSave()}>Kaydet</Button>
        </DialogActions>
    
    </div>
  )}