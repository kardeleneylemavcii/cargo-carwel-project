import React from "react";
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import { Container } from "@mui/material";
import AddPostCarrierEditDate from "./AddPostCarrierEditDate";
import '../../css/post-customer.css';

const AddPostCarrierEditForm = ({
  // selectMedia, 
  //setTempMedia,
  // tempMedia ,
  setDeparture,
  setArrival,
  setDepartureDate,
  setArrivalDate,
  setLoadWeight,
  setLoadSize,
  setLoadType,
  setPrice,
  departureDate,
  arrivalDate,
  cityList,
  setDescriptions
}) => {

  return (
    <Container sx={{ padding: 4, border: 3, borderColor: "#127d88", borderRadius: 5, mt:-3}}>
      <Typography variant="h6" gutterBottom>
        KAYIT FORM
      </Typography><br/>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <select         
            className={"my-select"}
            onChange={(e) => setDeparture([].slice.call(e.target.selectedOptions).map(item => {
              return { _id: item.value, name: item.text };
            }))}
          >
            <option value="">Kalkış Noktası Seçiniz</option>
            {cityList && cityList.map((city, i) => (
              <option key={i} value={city.id}>{city.name}</option>
            ))}
          </select>
        </Grid>
        <Grid item xs={12} sm={6} >
          <select
            className={"my-select"}
            onChange={(e) => setArrival([].slice.call(e.target.selectedOptions).map(item => {
              return { _id: item.value, name: item.text };
            }))}
          >
           <option value="">Varış Noktası Seçiniz</option>
            {cityList && cityList.map((city, i) => (
              <option key={i} value={city.id}>{city.name}</option>
            ))}
          </select>
          {/* <TextField
            required
            id="arrival"
            name="arrival"
            label="Yük teslim Edilecegi yer"
            fullWidth
            onChange={(e) => setArrival(e.target.value)}
            variant="standard"
          /> */}
        </Grid>
        <Grid item xs={12}  >
          <Grid item xs={12}  >
            <TextField
              required
              id="loadWeight"
              name="loadWeight"
              label="Yükün Ağırlıgı"
              fullWidth
              onChange={(e) => setLoadWeight(e.target.value)}
              variant="standard"
bg            />
          </Grid >
          <Grid item xs={12} >
            <TextField
              id="loadSize"
              name="loadSize"
              label="Yükün büyüklüğü"
              fullWidth
              variant="standard"
              onChange={(e) => setLoadSize(e.target.value)}
            />
          </Grid>
        </Grid>
        <Grid item xs={12} sm={12} sx={{mt:-3}} >
          <TextField
            required
            id="loadType"
            name="loadType"
            label="Yükün Türü"
            fullWidth
            autoComplete="shipping postal-code"
            variant="standard"
            onChange={(e) => setLoadType(e.target.value)}
          />
        </Grid>
        <Grid item xs={12} sm={12} sx={{mt:-3}} >
          <TextField
            required
            id="price"
            name="price"
            label="Fiyat"
            fullWidth
            autoComplete="shipping postal-code"
            variant="standard"
            onChange={(e) => setPrice(e.target.value)}
          />
        </Grid>
        <Grid item xs={12} md={12} sm={12} >
          <TextField
            multiline
            required
            id="descriptions"
            name="descriptions"
            label="Belirtmek istediğiniz ilan detaylarını bu alana yazınız"
            fullWidth
            onChange={(e) => setDescriptions(e.target.value)}
            variant="standard"
          />
        </Grid>
        <Grid item xs={12} sm={12} >
          <AddPostCarrierEditDate
            setDepartureDate={setDepartureDate}
            setArrivalDate={setArrivalDate}
            departureDate={departureDate}
            arrivalDate={arrivalDate}
          />
        </Grid>
        {/* <Grid item xs={12} sm={6}>
          <InputLabel htmlFor="outlined-adornment-amount"> Media yükleyiniz...</InputLabel>
          {
            tempMedia
              ?
              <div>
                <img src={tempMedia} style={{ width: "300px" }} />
                <Button
                  variant="text"
                  component="label"
                  onClick={() => setTempMedia()}
                >
                  Vazgeç
                </Button>
              </div>
              :
              <Button
                variant="text"
                component="label"
              >
                Select File
                <input

                  type="file"
                  
                  accept={'image/*, video/*, audio/*'}
                  hidden
                  onChange={(e) => selectMedia(e)}
                />
              </Button>

          }

        </Grid> */}
      </Grid>
    </Container>
  )
}
export default AddPostCarrierEditForm;