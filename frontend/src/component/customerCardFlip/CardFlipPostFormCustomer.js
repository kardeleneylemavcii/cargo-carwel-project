import React, { useState } from 'react';
import ReactBoxFlip from "react-box-flip";
import AddPostCustomer from './AddPostCustomer';
import { Button, Grid, Container } from '@mui/material';
import { ForkRightOutlined } from "@mui/icons-material"
import yükVeren from "../../img/yükVeren.jpg"

function CardFlipPostFormCarrier() {
    const [isFlipped, setIsFlipped] = useState(false);
    function handleClick() {
        setIsFlipped(!isFlipped);
    }
    return (
        <Container maxWidth="xl" sx={{ mb: -5, mr: -8 }}>
            <Button sx={{ mt: -9, ml: 15, backgroundColor: "#127d88", color: "#38263b" }} variant="outlined" color="secondary" onClick={handleClick}>
                Bireysel Yük İlan Kayıt Formu
                <ForkRightOutlined />
            </Button>
            <Grid container spacing={2}>
                <Grid item xs={12} md={3} >

                </Grid>
                <Grid item xs={12} md={10}>
                    <ReactBoxFlip isFlipped={isFlipped} >
                        <div>
                            <img className="pic" style={{ width: 500, height: 550, border: 2, borderRadius: 10, boxShadow: " 10px 10px 10px #000" }} src={yükVeren} alt="Jimmy Eat World" />
                        </div>
                        <div>
                            <AddPostCustomer />
                        </div>
                    </ReactBoxFlip>
                </Grid>
            </Grid>
        </Container>
    );
}

export default CardFlipPostFormCarrier;