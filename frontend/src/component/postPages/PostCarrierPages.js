import React, { useState, useEffect } from "react";
import { useOutletContext } from "react-router-dom";
import axios from "axios";
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import IconButton from '@mui/material/IconButton';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { Navigate } from "react-router-dom";
import nakliyat from "../../img/nakliyat.jpg";
import Moment from "moment";
import { Button, Grid } from "@mui/material";
import { useSelector } from "react-redux";
const ServerEndPoint = "http://localhost:8000/api/";

function PostCarrierPages() {
    const { departure, arrival } = useOutletContext();
    console.log(departure);
    console.log("arrivaldata", arrival);
    //const [page, setPage] = useState(0);
    //const [rowsPerPage, setRowsPerPage] = useState(10);  
    const [carrData, setCarrData] = useState(null);
    const [redirectUrl, setRedirectUrl] = useState(null);
    const [expanded, setExpanded] = useState(false);
    const [favorite, setFavorite] = useState([]);
    const [carrierListLoader, setCarrierListLoader] = useState(false);
    const [favoriteLoader, setFavoriteLoader] = useState(false);
    const userData = useSelector((state) => state.login.data)

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    useEffect(() => {
        let params;
        if (departure) {
            params = departure[0]._id;
        }
        if (arrival) {
            params += "/" + arrival[0]._id;
        }
        console.log(params);
        axios.get(ServerEndPoint + "carrier/carrierList/" + params).then((res) => {
            setCarrData(res.data.carrierList)
        }).catch(err => console.log(err))
    }, [carrierListLoader, departure]);

    const postRedirect = (type, _id) => {
        console.log(_id);
        setRedirectUrl('/postDetails/' + type + "/" + _id);
    }
    useEffect(() => {
        axios.get(ServerEndPoint + "favorite/list" + "/" + userData?.id)
            .then(res => {
                setFavorite(res.data)
            })
            .catch(err => console.log(err))
    }, [favoriteLoader])


    const handleFavori = (id) => {
        let data = {
            userId: userData?.id,
            postId: id
        }

        axios.post(ServerEndPoint + "favorite/save", data)
            .then(res => {
                //console.log(res);
                setFavoriteLoader(!favoriteLoader)
            })
            .catch(err => console.log(err))

    }
    if (redirectUrl) {
        return <Navigate to={redirectUrl} />
    }

    return (
        <div>
            <Grid container spacing={2} sx={{ padding: 12, mt: -8.7, mx: "auto" }} >
                {carrData && carrData.map((item, i) => (
                    <Grid item key={i} md={6} mt={-2} py={2}>
                        <Card sx={{ padding: 5, backgroundColor: " #527348", color: "rgb(248 232 229)", boxShadow: "3px 3px 3px 3px rgb(37 29 29)" }} >
                            <CardMedia
                                component="img"
                                sx={{
                                    justifyContent: "center",
                                    width: '100%',
                                }}
                                alt="The house from the offer."
                                src={nakliyat}
                            />
                            <CardContent>
                                <Grid container spacing={2} >
                                    <Grid item xs="5"> {item.departure.name}</Grid>
                                    <Grid item xs="1" md={-5}>-></Grid>
                                    <Grid item xs={5}> {item.arrival.name}</Grid>
                                    <Grid item xs={6}>Kalkış : {Moment(item.departureDate).format("DD-MM-YYYY HH:MM")}</Grid>
                                    <Grid item xs={6}>Varış  : {Moment(item.arrivalDate).format("DD-MM-YYYY HH:MM")}</Grid>
                                    <Grid item xs={12} sx={{ textAlign: "right !important" }}>Fiyat : {item.price}</Grid>
                                </Grid>
                            </CardContent>
                            <CardActions spacing={10} sx={{ mt: -4, ml: 8 }}>
                                <Button variant="contained" color="secondary" sx={{ ml: 12 }} onClick={() => postRedirect("posts", item._id)}>Teklif ver</Button>
                                {
                                    <IconButton aria-label="add to favorites" onClick={() => handleFavori(item._id)}>
                                        <FavoriteIcon style={{ mt: 10 }} sx={favorite?.map((fav) => ({
                                            color: fav.postId === item._id &&
                                                (fav.status ? "#9c27b0" : "gray")
                                        }))} />
                                    </IconButton>
                                }
                            </CardActions>
                        </Card>
                    </Grid>
                ))
                }
            </Grid>
        </div>
    );
}
export default PostCarrierPages;
