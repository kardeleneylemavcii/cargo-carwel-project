import React, { useEffect, useState } from "react";
import axios from "axios";
import { Grid, Button } from "@mui/material";
import { styled } from '@mui/material/styles';
import sortObjectsArray from "sort-objects-array";
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import nakliyat from "../../img/nakliyat.jpg";
import yükVeren from "../../img/yükVeren.jpg";
import karmaşa from "../../img/karmaşa.jpg";
import Moment from "moment";
import { useSelector } from "react-redux"
import { Navigate } from "react-router-dom";
const ServerEndPoint = "http://localhost:8000/api/";

const ExpandMore = styled((props) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));

const PostAll = () => {
    const [page, setPage] = useState(false);
    const [cutsData, setCutsData] = useState(null);
    const [carrData, setCarrData] = useState(null);
    const [allList, setAllList] = useState(null);
    const [favorite, setFavorite] = useState([]);
    const [carrierListLoader, setCarrierListLoader]=useState(false);
    const [favoriteLoader, setFavoriteLoader] = useState(false);
    const userData = useSelector((state) => state.login.data)
        const [redirectUrl, setRedirectUrl] = useState(null);
    const handleItem=(_id)=>{

    console.log(_id);

    }
    
   
     useEffect(() => {
       

        axios.get(ServerEndPoint + "customer/custsList").then((res) => {
            //console.log(res);
            setCutsData(res.data.customerList);

        }).catch(err => console.log(err))
    }, []);

 
    useEffect(() => {
        let params;
        axios.get(ServerEndPoint + "carrier/carrierList/"+params).then((res) => {
            // console.log(res);
            setCarrData(res.data.carrierList)
            //console.log(res)
        }).catch(err => console.log(err))
    }, []);

    useEffect(() => {



        
        if (cutsData && carrData) {
            let newArr = carrData.concat(cutsData);
            let karısık = sortObjectsArray(newArr, 'departureDate');
            //console.log(karısık)
            setAllList(karısık);
        }
    }, [cutsData, carrData])

    useEffect(() => {
     
        axios.get(ServerEndPoint + "favorite/list" + "/" + userData?.id)
            .then(res => {
                //console.log(":::::::::::::::::::::::::", res)
                setFavorite(res.data)
            })
            .catch(err => console.log(err))
    }, [favoriteLoader])


    const handleFavori = (id) => {
 
        let data = {
            userId: userData?.id,
            postId: id
        }

        axios.post(ServerEndPoint + "favorite/save", data)
            .then(res => {
                //console.log(res);
                setFavoriteLoader(!favoriteLoader)
            })
            .catch(err => console.log(err))

    }
    const postRedirect = (type, _id) => {
        //console.log(_id);
        setRedirectUrl('/postDetails/' + type + "/" + _id);

    }
    if (redirectUrl) {
        return <Navigate to={redirectUrl} />
    }


    return (
        <div>
           <Grid container spacing={2} sx={{ padding: 12, mt: -8.7, mx: "auto" }} >
                {allList && allList.map((item, i) => (
                    <Grid item key={i} md={4} mt={-2} py={2}>
                        <Card sx={{ maxWidth: 340, padding: 5, height: 400, backgroundColor: "#6f986d",color:"rgb(248 232 229)" , boxShadow: "3px 3px 3px 3px rgb(37 29 29)" }} >
                            <CardMedia
                                component="img"
                                sx={{
                                    justifyContent: "center",
                                    height: 150,
                                    width: 350,
                                }}
                                alt="The house from the offer."
                                src={karmaşa}
                            />
                        <CardContent >
                            <Typography paragraph component={'span'} variant={'body2'} >
                                <h4>Kalkış Noktası : {item.departure && item.departure.name}</h4>
                                <h4>Varış Noktası : {item.arrival && item.arrival.name}</h4>
                                <h4>Kalkış Zamanı : {Moment(item.departureDate).format("DD-MM-YYYY HH:MM")}</h4>
                                <h4>Varış  Zamanı : {Moment(item.arrivalDate).format("DD-MM-YYYY HH:MM")}</h4>
                                <h4>Fiyat : {item.price}</h4>                               
                            </Typography>
                        </CardContent>                        
                        <CardActions spacing={10} sx={{mt:-4, ml:8}}>
                        <Button variant="contained" color="secondary" sx={{ml:12}} onClick={() => postRedirect("posts", item._id)   }>Teklif ver</Button>
                        {
                            <IconButton aria-label="add to favorites" onClick={() => handleFavori(item._id)}>
                                        <FavoriteIcon sx={favorite?.map((fav) => ({
                                            color: fav.postId === item._id &&
                                                (fav.status ? "#9c27b0" : "gray")
                                        }))}/>
                                    </IconButton>
                                }                           
                        </CardActions>
                    </Card>
                    </Grid>
                ))
                }
            </Grid>
        </div>

    )
}
export default PostAll;