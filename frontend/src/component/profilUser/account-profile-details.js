import React, { useState, useEffect } from 'react';
import {Box,Button,Card,CardContent,CardHeader,Divider,Grid,TextField} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import axios from 'axios';
import config from '../../config';



export const AccountProfileDetails = ({ data, setName, setSurName, setEmail, handleUpdate }) => {

  const [open, setOpen] = useState(false);
  const [pass, setPass] = useState(null);
  const [confirmPass, setConfirmPass] = useState(null);
  const [isValid, setIsValid] = useState(false);
  const [error, setError] = useState({
    msg:"",
    err:false
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };



  useEffect(() => {
    if ( pass !== null && confirmPass !== null) {
      if(pass === confirmPass ){
         setError({
        msg:"dogrulandı!",
        err:false
      })
      } else {
        setError({
          msg:"Lütfen şifre tekrar giriniz!",
          err:true
        })
      }
     
    } else {
      setError({
        msg:"Lütfen şifre giriniz!",
        err:true
      })
    }

  }, [isValid])

  const updatePassword = () => {
    setIsValid(!isValid)
  if(error.err===false){
    console.log("click oldu")
    
    let password = {
      password: pass
    }
    axios.post(`${config.BACKEND_URL}profil/updatePass/${data?._id}`, password).then(res => {
    if (res) {
      alert("Tebrikler! Yeni İlanınız Başarıyla Kaydoldu!");
  }
}).catch(err => {
  console.log(err);
});
      
  }
  

  }
  return (

    <Card style={{}}>
      <CardHeader
        subheader="Profil Bilgileri Değişiklikleri"
        title="Profil Bilgileri"
      />
      <Divider />
      <CardContent>
        <Grid
          container
          spacing={3}
        >
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="İsminizi Giriniz"
              label="İsim"
              name="firstName"
              onChange={(e) => setName(e.target.value)}
              required
              defaultValue={data.name}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              fullWidth
              label="Soyisim"
              name="lastName"
              onChange={(e) => setSurName(e.target.value)}
              required
              defaultValue={data.surName}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              fullWidth
              label="Email Adresiniz"
              name="email"
              //onChange={(e) => setEmail(e.target.value)}
              required
             value={data.email}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          > <Button variant="outlined" onClick={handleClickOpen}>
              Şİfre değiştirme
            </Button>
            <Dialog open={open} onClose={handleClose}>
              <DialogTitle>Şifre Değiştirme</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  Şifrenizi yenilmek için formu doldurunuz!
                </DialogContentText>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="Şifre"
                  label="Şifre"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  onChange={(e) => setPass(e.target.value)}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="Şifre"
                  label="Şifrenizi tekrar giriniz"
                  type="password"
                  id="confirm-password"
                  autoComplete="current-password"
                  onChange={(e) => setConfirmPass(e.target.value)}

                />

                {
                  error.err ? <Box sx={{ color: 'error.main' }}>{error.msg}</Box>:<Box sx={{ color: 'error.main' }}>{error.msg}</Box>
                }



              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose}>İptal</Button>
                <Button onClick={updatePassword}>Kaydet</Button>
              </DialogActions>
            </Dialog>
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >

          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >

          </Grid>
        </Grid>
      </CardContent>
      <Divider />
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'flex-end',
          p: 2
        }}
      >
        <Button
          color="primary"
          variant="contained"
          onClick={() => handleUpdate()}
        >
          Bilgilerimi Değiştir
        </Button>
      </Box>
    </Card>

  );
};
