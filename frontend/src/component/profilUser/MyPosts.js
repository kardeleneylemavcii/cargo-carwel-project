import React, { useState, useEffect } from "react";
//import Moment from "moment";
import { Paper, Button } from "@mui/material";
import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody} from "@mui/material";
import { useSelector } from "react-redux";
import axios from "axios";
import config from '../../config';
import sortArray from "sort-array";
import Moment from "moment";
import Typography from '@mui/material/Typography';
function MyPosts() {

    const userPosts = useSelector((state) => state.login.data);
    const [customerData, setCustomerData] = useState(null);
    const [carrierData,setCarrierData]=useState(null);
    const[allList,setAllList]=useState(null);

    useEffect(() => {
        allCustomerFind();
        allCarrierFind();
    }, []);

    const allCustomerFind = () => {
        let guery = {
            _id: userPosts.id
        }
        axios.get(`${config.BACKEND_URL}customer/find/${ userPosts.id}`).then((res) => {
           // console.log("==================", res);
            setCustomerData(res.data)
        }).catch(err => console.log(err))
    }
 
  
    
    const allCarrierFind=()=>{
        let guery = {
            _id: userPosts.id
        }
         axios.get(`${config.BACKEND_URL}carrier/find/${ userPosts.id}`).then((res) => {
           // console.log("datata:::::::::",res);
            setCarrierData(res.data)
        }).catch(err => console.log(err))
    }
    
    useEffect(() => {
        if (customerData && carrierData) {
            let newArr = customerData.concat(carrierData);
                                                        //(!!!!!!!!!!!!!!dön) 
              newArr=sortArray(newArr,{
                by:"arrival",
                order:"name"
                }) 
            setAllList(newArr);
        }
    }, [customerData, carrierData])

  

    return (
        <>
        {
            (allList?.length>0)? <TableContainer component={Paper}>
    <Table sx={{ maxwidth: "12", minWidth: 12 }} aria-label="simple table">
        <TableHead sx={{ backgroundColor: "#827717" }}>
            <TableRow>
                         <TableCell><h3>Yük Alınacak Yer</h3></TableCell>
                        <TableCell aligin="center"><h3>Yük Teslim Edilecek Yer</h3></TableCell>
                        <TableCell aligin="center"><h3>Gidiş Tarihi</h3 ></TableCell>
                        <TableCell aligin="center"><h3>Varış Tarihi</h3></TableCell>
                        <TableCell aligin="center"><h3>Yük Ağırlığı </h3></TableCell>
                        <TableCell aligin="center"><h3>Yük Büyüklüğü(kg)</h3></TableCell>
                        <TableCell aligin="center"><h3>Yük Türü</h3></TableCell>
                        <TableCell aligin="center"><h3>Kayıt Tarihi</h3></TableCell>

                <TableCell aligin="center"><h3>KAYIT SİL</h3></TableCell>
            </TableRow>
        </TableHead>
        <TableBody>

                  
        {allList&&allList.map((item, i) => (
                            <TableRow
                                key={i}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}                            >
                               <TableCell >{item.departure.name}</TableCell>
                                <TableCell >{item.arrival.name}</TableCell>
                                <TableCell >{Moment(item.departureDate).format("DD-MM-YYYY HH:MM")}</TableCell>
                                <TableCell >{Moment(item.arrivalDate).format("DD-MM-YYYY HH:MM")}</TableCell>
                                <TableCell >{item.loadWeight ? item.loadWeight : "Bilinmiyor"}</TableCell>
                                <TableCell >{item.loadSize}</TableCell>
                                <TableCell >{item.loadType}</TableCell>
                                <TableCell >
                                    {Moment(item.createdAt).format("DD-MM-YYYY HH:MM")}    
                                </TableCell>
                                <TableCell >
                                    <Button variant="contained" color="secondary" /* onClick={()=>handleDelCLick(item._id)} */>İlanı Kaldır!</Button></TableCell>
                            </TableRow>
                        ))

                    }
        </TableBody>
    </Table>
 
</TableContainer>:<Typography paragraph component={'span'} variant={'body2'}>İlan eklemediniz!</Typography>
        }
   </>
    )
}
export default MyPosts;