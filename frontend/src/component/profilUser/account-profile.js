import React, { useState } from 'react';
import {Avatar,Box,Button,Card,CardActions,CardContent,Divider,Typography} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import config from '../../config';

export const AccountProfile = ({ tempMedia, selectMedia, setTempMedia, handleNewsSubmit, data }) => {
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setTempMedia(null)
  };
  return (
    <Card >
      <CardContent>
        <Box
          sx={{
            alignItems: 'center',
            display: 'flex',
            flexDirection: 'column'
          }}
        >
          <Avatar
            src={`${config.BACKEND_BASEPATH}${data?.media?.path}`}
            sx={{
              height: 64,
              mb: 2,
              width: 64
            }}
          />
          <Typography
            color="textPrimary"
            gutterBottom
            variant="h5"
          >
            {data.name}
          </Typography>
          <Typography
            color="textSecondary"
            variant="body2"
          >
            {`${data.surName}`}
          </Typography>
          <Typography
            color="textSecondary"
            variant="body2"
          >
            {data.email}
          </Typography>
        </Box>
      </CardContent>
      <Divider />
      <CardActions>
        <Button
          color="primary"
          fullWidth
          variant="text"
          onClick={handleClickOpen}
        >
          Profil Resmi Yükle
        </Button>
      </CardActions>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >


        <DialogTitle id="alert-dialog-title">
          Bir Resim seçiniz!
        </DialogTitle>
        <DialogContent >
          <DialogContentText id="alert-dialog-description">
            {
              tempMedia
                ?
                <img src={tempMedia} style={{ width: "400px", marginTop: 50, marginLeft: 10 }} /> : <img style={{ width: "300px" }} src={`${config.BACKEND_BASEPATH}${data?.media?.path}`} />
            }
          </DialogContentText>
        </DialogContent>
        <DialogActions sx={{width:"600px"}} >
          <Button variant="contained" component="label" sx={{ width: "100%" }} onClick={() => handleNewsSubmit()}>
            kaydet
          </Button>
          {
            tempMedia ?
              <Button
                variant="contained"
                component="label"
                sx={{ width: "100%" }}
                onClick={() => setTempMedia()}
              >
                Vazgeç
              </Button> : <Button
                variant="contained"
                component="label"
                sx={{ width: "100%" }}
              >
                Bir Resim Yükle
                <input

                  type="file"
                  /* multiple */
                  accept={'image/*, video/*, audio/*'}
                  hidden
                  onChange={(e) => selectMedia(e)}
                />
              </Button>
          }
 
              <Button
                variant="contained"
                component="label"
                sx={{ width: "100%" }}
                onClick={() => setOpen(false)}
              >
                Kapat
              </Button>

        </DialogActions>







      </Dialog>
    </Card>
  )
};
