import React, { useState, useEffect } from "react";
import axios from "axios";
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { Navigate } from "react-router-dom";
import nakliyat from "../../img/nakliyat.jpg";
import Moment from "moment";
import { Button, Grid } from "@mui/material";
import { useSelector } from "react-redux";
const ServerEndPoint = "http://localhost:8000/api/";

function MyFavorite() {

    const userData = useSelector((state) => state.login.data);
    const [favorite, setFavorite] = useState([]);
    const [result, setResult] = useState(null);
    const [favoriteLoader, setFavoriteLoader] = useState(false);
    const [allList, setAllList] = useState(null);
    const [redirectUrl, setRedirectUrl] = useState(null);

    useEffect(() => {
        axios.get(ServerEndPoint + "favorite/list" + "/" + userData?.id)
            .then(res => {
                console.log(":::::::::::::::::::::::::", res.data)
                setFavorite(res.data);
            })
            .catch(err => console.log(err));
    }, [favoriteLoader]);

    useEffect(() => {
        let result = favorite.filter(item=>item.status===true).map(item =>item.postId);
       
        setResult(result);
      
    }, [favorite])

   
    useEffect(() => {        
  if (result !== null && result.length > 0) {
    console.log("result o sa bu nıye array ",result)
            axios.post(`${ServerEndPoint}posts/all`, result)
                .then(res => {
                    //console.log("----------------",res)
                setAllList(res.data)
                    //ilanları set edeceksin burdan
                })
                .catch(err => console.log(err))
        }else{
            //console.log("===========",result)
            setAllList(null)
        }
    }, [result,favoriteLoader])

    const handleFavori = (id) => {
        let data = {
            userId: userData?.id,
            postId: id
        }

        axios.post(ServerEndPoint + "favorite/save", data)
            .then(res => {
                //console.log(res);
                setFavoriteLoader(!favoriteLoader) 
            })
            .catch(err => console.log(err))          
    }
    const postRedirect = (type, _id) => {
        console.log(_id);
        setRedirectUrl('/postDetails/' + type + "/" + _id);
    }
    if (redirectUrl) {
        return <Navigate to={redirectUrl} />
    }
    return (
        <div>
            <Grid container spacing={3} sx={{ mt: -2, mx: "auto" }}  >
                {allList ? allList
                    .map((item, i) => (
                        <Grid item key={i} md={4} mt={-2} py={2}>
                            <Card xs={12} md={6} lg={4} sx={{ maxWidth: 345,padding: 0, height: 455, ml:-3, color:"rgb(248 232 229)", backgroundColor:"#6f986d", boxShadow: "3px 3px 3px 3px rgb(37 29 29)" }}>
                                <CardMedia
                                    component="img"
                                    sx={{
                                        justifyContent: "center",
                                        height: 150,
                                        width: 350,
                                    }}
                                    alt="The house from the offer."
                                    src={nakliyat}
                                />
                                <CardContent>
                                    <Typography paragraph component={'span'} variant={'body2'}>
                                        <h4>Kalkış Noktası : {item.departure && item.departure.name}</h4>
                                        <h4>Varış Noktası : {item.arrival && item.arrival.name}</h4>
                                        <h4>Kalkış Zamanı : {Moment(item.departureDate).format("DD-MM-YYYY HH:MM")}</h4>
                                        <h4>Varış  Zamanı : {Moment(item.arrivalDate).format("DD-MM-YYYY HH:MM")}</h4>
                                        <h4>Fiyat : {item.price}</h4>
                                    </Typography>
                                </CardContent>
                                <CardActions>                                  
                                <Button variant="contained" color="secondary" sx={{ml:21}} onClick={() => postRedirect("posts", item._id)   }>Teklif ver</Button>
                                        {
                                            <IconButton aria-label="add to favorites" onClick={() => handleFavori(item._id)}>
                                                <FavoriteIcon sx={favorite?.map((fav) => ({
                                                    color: fav.postId === item._id &&
                                                        (fav.status ? "#9c27b0" : "gray")
                                                }))} />
                                            </IconButton>
                                        }
                                </CardActions>
                                <Collapse timeout="auto" unmountOnExit>
                                    <CardContent>
                                        asdas
                                    </CardContent>
                                </Collapse>
                            </Card>
                        </Grid>
                    )): <Typography paragraph component={'span'} variant={'body2'}>Favorilere birşey eklemediniz!</Typography>
                }
            </Grid>
        </div>
    )}

export default MyFavorite;