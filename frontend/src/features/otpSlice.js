import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    data: null
}

export const otpSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setOtp: (state, action) => {
            //console.log("action payload:::",action.payload)
            if (action.payload === null) {
                state.data = null;
            } else {
                // Object.assign(state.data,action.payload)
                state.data = action.payload
            }

            //console.log("state data slice",state.data)
        }
    },
})

// Action creators are generated for each case reducer function
export const { setOtp } = otpSlice.actions
export default otpSlice.reducer;