import {createSlice} from '@reduxjs/toolkit'

const initialState = {
  data: undefined
}

export const loginSlice = createSlice({
  name: 'login',
  initialState,
  reducers: {
    setUser: (state, action) => {
      //console.log("action payload:::",action.payload)
      if (action.payload === undefined) {
        state.data = undefined;
      } else {
        // Object.assign(state.data,action.payload)
        state.data = action.payload
      }
    //console.log("state data slice",state.data)
    }
  },
})

// Action creators are generated for each case reducer function
export const {setUser} = loginSlice.actions
export default loginSlice.reducer;