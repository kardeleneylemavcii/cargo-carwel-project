import React, { useState} from "react";
import { useNavigate } from 'react-router-dom';
import axios from "axios";
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import { createTheme } from '@mui/material/styles';
import AccountDialog from "../component/account/AccountDialog";
import { Dialog } from "@mui/material";
import { useDispatch, useSelector } from 'react-redux';
import { setUser } from '../features/loginSlice';
import { setChatRoom } from "../features/chatRoomSlice";
const ServerEndPoint = "http://localhost:8000/api/";

const theme = createTheme({
  backgroundColor: "red"
});



const SignIn = () => {
  const [dialog, setDialog] = useState(false)
  const [email, setEmail] = useState(null);
  const [pass, setPass] = useState(null);

  const userData = useSelector((state) => state.login.data);

  //const [user, setUser] = useState(null);
  const navigate = useNavigate();

  const dispatch = useDispatch()
  const singIn = () => {

    axios.get(ServerEndPoint + "auth/login/" + email + '/' + pass).then(res => {
      console.log(res.data.user);
      dispatch(setUser(res.data.user));

      localStorage.setItem("token", res.data.token);
      navigate("/")

    }).catch(err => console.log(err))
  }

  if (userData) {

    let senderId = userData?.id
    axios.get(ServerEndPoint + "chat/getUserMsg/" + senderId)
      .then(res => {
        dispatch(setChatRoom(res.data))

      })
      .catch(err => console.log(err))
  }

  ///console.log("signİn:::",currentUser)
  return (

    <Box
      component="main" maxWidth="xxl"
      sx={{
        backgroundColor: (theme) =>
          theme.palette.mode === 'light'
            ? theme.palette.grey[1000]
            : theme.palette.grey[1000],
        flexGrow: 1,
        height: '141vh',
        overflow: 'auto',
        padding: "0px 0px 0px 0px",
        backgroundImage: `url(${require("../img/login.jpg")})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: "100% 100%",
        color: 'white',

      }}
    >
      <CssBaseline />
      <Box
        sx={{
          marginTop: '124px',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',

        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main', marginTop: '-50px' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="outlined" sx={{ color: '#9c27b0' }} >
          Giriş Yap
        </Typography>
        <Box sx={{ mt: 0 }}>
          <TextField
            margin="normal"
            variant="outlined"
            color="error"
            sx={{ color: 'yellow' }}
            required
            fullWidth
            id="email"
            label="Email Adresi"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            margin="normal"
            variant="outlined"
            color="error"
            required
            fullWidth
            name="password"
            label="Şifre"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(e) => setPass(e.target.value)}
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
            sx={{ mt: 3, mb: 2, borderRadius: '1px', border: "1px solid white", background: '#36a7b791' }}
            onClick={() => singIn()}

          >
            Giriş Yap
          </Button>
          <Grid container >
            <Grid item xs  >
              <Button href="/forgotPassword" variant="contained" color="secondary" sx={{ borderRadius: '1px', border: "1px solid white", background: '#36a7b791' }}>
                Şifrenizi mi Unuttunuz?
              </Button>
            </Grid>
            <Grid item>
              <Dialog
                open={dialog}
                onClose={() => setDialog(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <AccountDialog setDialog={setDialog} />
              </Dialog>
              <Button variant="contained" color="secondary" sx={{ borderRadius: '1px', border: "1px solid white", background: '#36a7b791' }} onClick={() => setDialog(true)}>
                {"Hesabınız mı Yok? Kayıt Olun"}
              </Button>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Box>



  )
}
export default SignIn;