import React, { useState } from 'react'
import CardFlipPostFormCarrier from "../component/carrierCardFlip/CardFlipPostFormCarrier";
import CardFlipPostFormCustomer from "../component/customerCardFlip/CardFlipPostFormCustomer";
import { Grid, Container } from "@mui/material";

function AddPost() {
    const [isFlipped, setIsFlipped] = useState(false);

    function handleClick() {
        setIsFlipped(!isFlipped);
    }
    return (
        <Container maxWidth="xxl" sx={{ backgroundColor: " rgb(141 110 99 / 53%)", display: "flex", p: 5 }}>
            <Grid container spacing={2} sx={{ justifyContent: "center", marginTop: 0, p: 2, flex: "row" }} >
                <Grid item md={6} xs={12} sx={{ width: "100px", height: 800 }} >
                    <CardFlipPostFormCustomer />
                </Grid>
                <Grid item md={6} xs={12} sx={{ width: "100px", height: 800 }}>
                    <CardFlipPostFormCarrier isFlipped={isFlipped} handleClick={handleClick} />
                </Grid>
            </Grid>
        </Container>
    )
}

export default AddPost;