
import React from "react";
import { Grid } from "@mui/material";
import AddPostCarrCard from "../component/addPostPage/AddPostCarrCard";
import AddPostCutsCard from "../component/addPostPage/AddPostCutsCard";

import AddPostLayout from "../component/addPostPage/AddPostLayout";







function AddPostPage() {

    return (
        <div>

            <AddPostLayout >

                <Grid
                    container
                    spacing={2}
                    direction="row"
                    justifyContent="space-evenly"
                    alignItems="center"
                >
                    <Grid item xs={12} md={6}>
                        <AddPostCutsCard />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <AddPostCarrCard />
                    </Grid>
                </Grid>


            </AddPostLayout>




        </div>
    );
}
export default AddPostPage;