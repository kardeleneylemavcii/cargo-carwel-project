
import React from 'react';
import { Box, Drawer, Divider, List, ListItem } from '@mui/material';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListItemButton from '@mui/material/ListItemButton';
import { Outlet, useNavigate } from 'react-router-dom'
import PersonIcon from '@mui/icons-material/Person';
import MessageIcon from '@mui/icons-material/Message';
import LocalShippingRoundedIcon from '@mui/icons-material/LocalShippingRounded';
import StarOutlineRoundedIcon from '@mui/icons-material/StarOutlineRounded';
import { styled, useTheme } from '@mui/material/styles';
import HomeIcon from '@mui/icons-material/Home';
import InfoIcon from '@mui/icons-material/Info';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import PostAddIcon from '@mui/icons-material/PostAdd';
import Typography from '@mui/material/Typography';
import LogoutIcon from '@mui/icons-material/Logout';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import { useDispatch } from "react-redux"
import { setUser } from '../features/loginSlice';
const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  }),
)
const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));
const ProfileSideBar = () => {
  const navigate = useNavigate();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  const signOut = () => {
    dispatch(setUser(null));
    localStorage.removeItem("token");
    navigate("/login");
  }
  return (



    <Box sx={{ display: 'flex' }}>
      <CssBaseline />

      <AppBar position="fixed" open={open} sx={{backgroundColor:"#8d6e63"}}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{ mr: 2, ...(open && { display: 'none' }) }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div">
            User Profil
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
        variant="persistent"
        anchor="left"
        open={open}
      >
        <DrawerHeader>     
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List>
          <ListItemButton onClick={() => navigate("profil")}>
            <ListItemIcon >
              <PersonIcon />
            </ListItemIcon>
            <ListItemText primary="Profil " />
          </ListItemButton>
          <ListItemButton onClick={() => navigate("myPosts")}>
            <ListItemIcon >
              <LocalShippingRoundedIcon />
            </ListItemIcon>
            <ListItemText primary="İlanlarım" />
          </ListItemButton>

          <ListItemButton onClick={() => navigate("myFavorite")}>
            <ListItemIcon >
              <StarOutlineRoundedIcon />
            </ListItemIcon>
            <ListItemText primary="Favoriler " />
          </ListItemButton>

          <ListItemButton onClick={() => navigate("Yerine başka birşey gelecek")}>
            <ListItemIcon >
              <MessageIcon />
            </ListItemIcon>
            <ListItemText primary="Mesajlarım " />
          </ListItemButton>
        </List>
        <Divider />
        <List>  
          <ListItem  disablePadding>
            <ListItemButton onClick={() => navigate("/")}>
              <ListItemIcon>
                <HomeIcon/>
              </ListItemIcon>
              <ListItemText  primary="Home "/>
            </ListItemButton>
          </ListItem>
     
          <ListItem  disablePadding>
            <ListItemButton onClick={() => navigate("/about")}>
              <ListItemIcon>
                <InfoIcon/>
              </ListItemIcon>
              <ListItemText  primary="About "/>
            </ListItemButton>
          </ListItem>

          <ListItem  disablePadding>
            <ListItemButton onClick={() => navigate("/post")}>
              <ListItemIcon>
                <LocalShippingIcon/>
              </ListItemIcon>
              <ListItemText  primary="İlanlar "/>
            </ListItemButton>
          </ListItem>

          <ListItem  disablePadding>
            <ListItemButton onClick={() => navigate("/addPost")}>
              <ListItemIcon>
                <PostAddIcon/>
              </ListItemIcon>
              <ListItemText  primary="ilan Ver "/>
            </ListItemButton>
          </ListItem>

          <ListItem  disablePadding>
            <ListItemButton onClick={signOut}>
              <ListItemIcon>
                <LogoutIcon/>
              </ListItemIcon>
              <ListItemText  primary="Çıkış "/>
            </ListItemButton>
          </ListItem>



        </List>
     
       
      </Drawer>
      
      <Main open={open}>
        <DrawerHeader />
        <Outlet />
       
      </Main>
      
    </Box>

    

  );
}





export default ProfileSideBar;