import React, { useState } from 'react';
import { Outlet, useNavigate } from "react-router-dom";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import { useSelector, useDispatch } from "react-redux"
import { setUser } from '../features/loginSlice';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import Badge from '@mui/material/Badge';
import NotificationsIcon from '@mui/icons-material/Notifications';
import Footer from "./Footer";
import { Grid } from "@mui/material"
import axios from "axios";
import AdbIcon from '@mui/icons-material/Adb';
import MenuIcon from '@mui/icons-material/Menu';
import ListItemButton from '@mui/material/ListItemButton';
const ServerEndPoint = "http://localhost:8000/api/";

function MainLayout() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [anchorElUser, setAnchorElUser] = useState(null);
  const [anchorElNav, setAnchorElNav] = useState(null);

  const chatData = useSelector(state => state.chatRoom.data);
  const result = chatData.filter(item => item.seen === false);

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };
  const userData = useSelector((state) => state.login.data);

  if (!userData && localStorage.getItem("token")) {
    console.log("userData not found");
    const recordToken = localStorage.getItem("token");
    axios.get(ServerEndPoint + "auth/withToken/" + recordToken).then(res => {
      console.log(res.data.user);
      dispatch(setUser(res.data.user));

      localStorage.setItem("token", res.data.token);
      navigate("/")

    }).catch(err => console.log(err))
  }
  const signOut = () => {
    dispatch(setUser(null));
    localStorage.removeItem("token");
    navigate("/login");
  }

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };


  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  return (
    <>
      <AppBar position="static" sx={{ backgroundColor: "#8d6e63", padding: 0 }}>
        <Container maxWidth="xxl" >
          <Toolbar disableGutters>
            <AdbIcon sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }} />
            <Typography
              variant="h6"
              noWrap
              component="a"
             href="/"
              sx={{
                mr: 2,
                display: { xs: 'none', md: 'flex' },
                fontFamily: 'monospace',
                fontWeight: 700,
                letterSpacing: '.3rem',
                color: 'inherit',
                textDecoration: 'none',
              }}
            >
              CARVEL
            </Typography>

            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleOpenNavMenu}
                color="inherit"
              >
                <MenuIcon />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorElNav}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
                open={Boolean(anchorElNav)}
                onClose={handleCloseNavMenu}
                sx={{
                  display: { xs: 'block', md: 'none' },
                }}
              >

                <MenuItem onClick={handleCloseNavMenu}>
                <ListItemButton onClick={() => navigate("/")}>
                  <Typography textAlign="center" >Home</Typography>
                  </ListItemButton>
                  
                </MenuItem>
                <MenuItem onClick={handleCloseNavMenu}>
                <ListItemButton onClick={() => navigate("/about")}>
                    <Typography textAlign="center" >About</Typography>
                  </ListItemButton>
                
                </MenuItem>
                <MenuItem onClick={handleCloseNavMenu}>
                <ListItemButton onClick={() => navigate("/post")}>
                  <Typography textAlign="center"  >  İlanlar</Typography>
                  </ListItemButton>
                  
                </MenuItem>
                {userData && (<MenuItem onClick={handleCloseNavMenu}>
                  <ListItemButton  onClick={() => navigate("/addPost")}>
                   <Typography textAlign="center">İlan ver</Typography>
                </ListItemButton>
                 
                </MenuItem>
                )}

              </Menu>
            </Box>
            <Typography
              variant="h5"
              noWrap
              component="a"
              href=""
              sx={{
                mr: 2,
                display: { xs: 'flex', md: 'none' },
                flexGrow: 1,
                fontFamily: 'monospace',
                fontWeight: 700,
                letterSpacing: '.3rem',
                color: 'inherit',
                textDecoration: 'none',
              }}
            >
             CARVEL
            </Typography>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
              <Button
                sx={{ my: 2, color: 'white', display: 'block' }}
                onClick={() => navigate("/about")}
              >
                About

              </Button>

              <Button
                sx={{ my: 2, color: 'white', display: 'block' }}
                onClick={() => navigate("/post")}
              >
                İlanlar

              </Button>

              {userData && (
                <Button
                  sx={{ my: 2, color: 'white', display: 'block' }}
                  onClick={() => navigate("/addPost")}
                >
                  ilan Ver
                </Button>


              )}

            </Box>
            <Box sx={{ flexGrow: 0 }}>

              {userData && (
                <IconButton color="inherit" sx={{ p: 2 }} >
                  <Badge badgeContent={result?.length} color="secondary"  title="mesajınız var">
                    <NotificationsIcon />
                  </Badge>
                </IconButton>
              )}

              <Tooltip title="Open settings">
                {(!userData) ?
                  <Button
                    sx={{ my: 2, color: 'white', display: 'block' }}
                    onClick={() => navigate("/login")}
                  >
                    SignIn

                  </Button>
                  :
                  <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                    <Avatar alt="Remy Sharp" >{userData.name && JSON.stringify(userData.name).slice(1, 2).toUpperCase()}</Avatar>
                  </IconButton>}
              </Tooltip>
              <Menu
                sx={{ mt: '45px' }}
                id="menu-appbar"
                anchorEl={anchorElUser}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu}
              >

                <MenuItem onClick={handleCloseUserMenu}>
                <ListItemButton onClick={() => navigate("/profil")}>
                  <Typography textAlign="center" >Profil</Typography>
                  </ListItemButton>
                </MenuItem>
                <MenuItem onClick={handleCloseUserMenu}>
                <ListItemButton>
                   <Typography textAlign="center">HelloWorld</Typography>
                </ListItemButton>
                
                </MenuItem>
                <MenuItem onClick={handleCloseUserMenu}>
                <ListItemButton onClick={signOut}>
                  <Typography textAlign="center" >Out</Typography>
                  </ListItemButton>
                </MenuItem>
              </Menu>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      <Grid component="div" >
        <Outlet />
        <Footer />
      </Grid>
    </>
  );
}
export default MainLayout;