import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';
import axios from "axios";
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { useDispatch } from 'react-redux';
import { setAdmin } from "../features/authSlice";
import {useSelector} from "react-redux"


const ServerEndPoint = "http://localhost:8000/api/";
export default function AdminSignIn() {

  const [email, setEmail] = useState(null);
  const [pass, setPass] = useState(null);

  const adminData = useSelector((state) => state.admin.data);
  console.log(adminData)

  const navigate = useNavigate();

  const dispatch = useDispatch()
 
///console.log("

  const handleSubmit = (event) => {
    event.preventDefault();
   axios.get(ServerEndPoint + "auth/authLogin/" + email + '/' + pass+"/admin").then(res => {
    console.log(res)
      dispatch(setAdmin(res.data.user));
      localStorage.setItem("token", res.data.token);
      navigate("/admin")
    }).catch(err => console.log(err))
  };

  return (

      <Container component="main" maxWidth="xs" sx={{

        padding: 5,

      }}>
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
         
          <Typography component="h1" variant="h5">
            Admin Sign in
          </Typography>
          <Box noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={(e) => setEmail(e.target.value)}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={(e) => setPass(e.target.value)}
            />
            
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              onClick={(event) => handleSubmit(event)}
            >
              Sign In
            </Button>
            
          </Box>
        </Box>

      </Container>

  );
}