import React, { useState } from "react";
import axios from "axios";
import styles from "./styles.module.css";

const ForgotPassword = () => {
    const [email, setEmail] = useState("");
    const [msg, setMsg] = useState("");
    const [error, setError] = useState("");

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const url = `http://localhost:8000/api/passwordReset/resetPassword`;
            const { data } = await axios.post(url, { email });
            setMsg(data.message);
            setError("");
        } catch (error) {
            console.log("HATAAAAA");
            if (
                error.response &&
                error.response.status >= 400 &&
                error.response.status <= 500
            ) {
                setError(error.response.data.message);
                setMsg("");
            }
        }
    };
    return (
        <div className={styles.container}>
            <form className={styles.form_container} onSubmit={handleSubmit}>
                <h1>Şifremi Unuttum</h1>
                <input
                    type="email"
                    placeholder="Email"
                    name="email"
                    onKeyPress={(e) => {
                        if (e.key === "Enter") {
                            setEmail(e.target.value);
                            ForgotPassword();
                        }
                    }}
                    onChange={(e) => setEmail(e.target.value)}
                    value={email}
                    required
                    className={styles.input}
                />
                {error && <div className={styles.error_msg}>{error}</div>}
                {msg && <div className={styles.success_msg}>{error}</div>}
                <button type="submit" className={styles.green_btn}>
                    Gönder
                </button>
            </form>
        </div>
    )
};

export default ForgotPassword;