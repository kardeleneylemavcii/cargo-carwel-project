/* import React, { useEffect, useState,useContext} from 'react';
import { TextField, Typography, Divider, List, ListItem, ListItemIcon, ListItemText, Grid, Paper } from '@mui/material';
import { Avatar } from '@mui/material';
import axios from "axios";
import config from '../config';
import ChatInput from '../component/chatPage/ChatInput';
import UserContext from '../context/UserContext';


const ChatUser = ({ email, selected, id, clickFunc }) => (
    <ListItem button key="RemySharp" selected={selected} onClick={() => clickFunc(id)}>
        <ListItemIcon >
            <Avatar alt="Remy Sharp" />
        </ListItemIcon>
        <ListItemText primary={email}>{email}</ListItemText>
    </ListItem>
);

const ChatMessage = ({ fromSelf, message }) => (
    <ListItem key="1">
        <Grid container>
            <Grid item xs={12}>
                <ListItemText align={fromSelf ? "right" : "left"} primary={message}>{message}</ListItemText>
            </Grid>
            <Grid item xs={12}>
                <ListItemText align="right" secondary="09:30"></ListItemText>
            </Grid>
        </Grid>
    </ListItem>
)


const Chat = () => {
    const [userList, setUserList] = useState([]);
    const [messages, setMessages] = useState([]);
    const {currentUser,setCurrentUser}=useContext(UserContext);
    
    useEffect(() => {
        axios.get(`${config.BACKEND_URL}user/getAllUser`)
            .then((res) => {
               console.log("user list get all ::::", res)
                if (res.status === 200) {
                    setUserList(res.data.map((item) => ({
                        ...item,
                        selected: false
                    })));

                } else if (res.status === 204) {
                    console.log("No user data!");
                }
            }).catch(err => console.log(err))
    }, []);
    
    useEffect(() => {
        console.log("user list changed:::", userList);
        console.log("user list selected true", userList.filter((item) => item.selected === true));
        getAllMessages();
    }, [userList])

    const getAllMessages = () => {
        let selectedUser = userList.filter((item) => item.selected === true)[0];

       console.log("userList useEffect  selectedUser :::::", selectedUser)
        if (selectedUser === undefined) {
            return;
        }
        axios.post(`${config.BACKEND_URL}chatMessage/getmsg`,
            {
                from: currentUser.id,
                to: selectedUser.id
            }).then((res) => {
                console.log("getMsg api response::::::", res);
                if (res.status === 200) {
                    setMessages(res.data);
                }
            }).catch(err => console.log(err)); 
    }

    const chatItemClick = (id) => {
        setUserList(userList.map((item) => {
            if (item.id === id) {
                item.selected = true;
            } else {
                item.selected = false;
            }
            return item;
        }))

    };
    const sendFunc = (text,successFunc) => {
        let selectedUser = userList.filter((user) => user.selected)[0];
        if (selectedUser === undefined) {
            return;
        }
        axios.post(`${config.BACKEND_URL}chatMessage/addmsg`, {
            from: currentUser.id,
            to: selectedUser.id,
            message: text
        }).then((res) => {
            console.log("senFunc:::addMSg res :::::: ", res)
            if(res.status===200){
                getAllMessages();
                successFunc();
            }
        }).catch(err => console.log(err))
    }

    return (
        <div>
            <Grid container>
                <Grid item xs={12} >
                    <Typography variant="h5" className="header-message">Chat</Typography>
                </Grid>
            </Grid>
            <Grid container component={Paper} sx={{ minWidth: 650 }}>
                <Grid item xs={3} sx={{ borderRight: '1px solid #e0e0e0' }}>
                    <List>


                    </List>
                    <Divider />
                    <Grid item xs={12} style={{ padding: '10px' }}>
                        <TextField id="outlined-basic-email" label="Search" variant="outlined" fullWidth />
                    </Grid>
                    <Divider />
                    <List>
                        {
                            userList.map((item, i) => <ChatUser key={i} {...item} clickFunc={chatItemClick} />)
                        }
                    </List>
                </Grid>
                <Grid item xs={9}>
                    <List sx={{
                        height: '70vh',
                        overflowY: 'auto'
                    }}>
                        {
                            messages.map((item, i) => <ChatMessage key={i} {...item} />)
                        }

                    </List>
                    <Divider />
                    <ChatInput sendFunc={sendFunc} />
                </Grid>
            </Grid>
        </div>
    );
}

export default Chat; */