import React, { useEffect, useState } from "react";
import axios from "axios";
import Moment from "moment";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import ChatMessage from "../component/chatPage/ChatMessage";
import { Grid, Container, Button, TextField, Fab, Avatar } from "@mui/material";
import { Forum, Close, Send } from '@mui/icons-material';
import '../css/chat.css';
import config from '../config';
import ws from '../lib/WsConfig';
import waitForOpenConnection from "../lib/WsWaitForConn";

const ServerEndPoint = "http://localhost:8000/api/";


function PostDetails() {
  const getParams = useParams();
  const [getPost, setGetPost] = useState(null);
  const [chatPanel, setChatPanel] = useState(false);
  const [text, setText] = useState("");
  const [loader, setLoader] = useState(false);
  const [messages, setMessages] = useState([]);
  const [customerListLoader, setCustomerListLoader] = useState(false);
  // mesagge post istegi buradan başlıyor


  const userData = useSelector((state) => state.login.data);

  const messageSend = () => {

    let data = {
      message: text,
      roomId: getParams._id,
      senderId: userData.id
    };
    if (getPost.user._id !== userData.id) {
      data.users = [userData.id, getPost.user._id]
    } else {
      if (messages.length) {
        data.users = messages[0].users
      }
    }

    axios.post(`${config.BACKEND_URL}chat/addmsg`, data).then((res) => {
      // console.log("senFunc:::addMSg res :::::: ", res)
      broadcast(res.data, group);
      setCustomerListLoader(!customerListLoader);
    }).catch(err => console.log(err));
  };


  useEffect(() => {
    getPostDetails();
  }, []);

  useEffect(() => {

    getRoomMessage(getParams._id);


  }, [customerListLoader]);

  // mesagge get istegi burada başlıyor
  const broadcast = (msg, room) => {

    ws.send(JSON.stringify({ room: room, msg: msg }))
  }


  const join = (room, socket) => {
    console.log(room, 'join');

    ws.send(JSON.stringify({ join: room, ws: socket }));
  }
  let socket = ws;
  const group = getParams._id;


  useEffect(() => {

    if (socket.readyState !== socket.OPEN) {
      try {
        waitForOpenConnection(socket).then(r => {
          join(group, socket);
        })
      } catch (err) {
        console.error(err)
      }
    } else {
      join(group, socket);

    }

    socket.onmessage = event => {

      if (event.data instanceof Blob) {
        const reader = new FileReader();
        reader.onload = () => {
          //console.log("Result: " + JSON.parse(reader.result));
          const getMessageData = JSON.parse(reader.result);
          //console.log(getMessageData);
          getRoomMessage(getMessageData.room);
          console.log(getMessageData.room);
        };
        reader.readAsText(event.data);
      } else {
        console.log("Result: " + event.data);
      }

      setCustomerListLoader(!customerListLoader);
    };
  }, [])


  const getRoomMessage = (_id) => {

    let query = {
      roomId: getParams._id,
    }

    if (messages.length) {
      query.users = { $in: messages[0].users };
    }

    axios.post(`${config.BACKEND_URL}chat/getmsg`, query).then((res) => {
      setMessages(res.data);

    }).catch(err => console.log(err));
  }
  console.log(messages)


  const getPostDetails = () => {
    let pathType = getParams._type;
    //console.log(getParams);
    axios.get(ServerEndPoint + pathType + "/post/" + getParams._id).then(res => {
      console.log("get post detail::::::::", res);
      setGetPost(res.data);
    }).catch(err => console.log(err));
  }



  useEffect(() => {
    axios.post(`${config.BACKEND_URL}chat/getSeenMsg/${getParams._id}`).then(res => {
      console.log("get seen detail::::::::", res);

    }).catch(err => console.log(err));

  }, [loader])




  return (
    <Container>
      {getPost &&

        <Grid container spacing={2}>
          <Grid item md={12} xs={12}>
            <h1 align="center">İlan Başlığı</h1>
          </Grid>
          <Grid item md={7} xs={12}>
            {
              getPost  /* &&  userData.id === getPost.user._id  */
                ?

                <Button variant="outlined" color="secondary" onClick={() => {
                  messages.length && setChatPanel(true)
                  setLoader(!loader)
                }}>
                  {messages.length ? "Sohbet katıl" : "Henüz Teklif Yok"}
                  <Forum sx={{ marginLeft: "10px" }} /> {messages.length}
                </Button>
                :
                <Button variant="outlined" color="secondary" onClick={() => setChatPanel(true)}>
                  Teklif için sohbet başlat
                  <Forum sx={{ marginLeft: "10px" }} />
                </Button>
            }
            <h4 align="right">İlan
              Tarihi:{getPost ? Moment(getPost.createdAt).format("DD-MM-YYYY HH:MM") : "Bilinmiyor"}</h4>
            <hr />
            <h2>Kalkış Yeri:{getPost ? getPost.departure.name : "Bilinmiyor"}</h2>
            <h2>Kalkış Zamanı:{getPost ? Moment(getPost.departureDate).format("DD-MM-YYYY HH:MM") : "Bilinmiyor"}</h2>
            <h2>Varış Yeri:{getPost ? getPost.arrival.name : "Bilinmiyor"}</h2>
            <h2>Varış Zamanı:{getPost ? Moment(getPost.arrivalDate).format("DD-MM-YYYY HH:MM") : "Bilinmiyor"}</h2>
            <h2>Yük Ağırlığı:{getPost ? getPost.loadWeight : "Bilinmiyor"}</h2>
            <h2>Yük Büyüklüğü:{getPost ? getPost.loadSize : "Bilinmiyor"}</h2>
            <h2>Yük Türü:{getPost ? getPost.loadType : "Bilinmiyor"}</h2>
            <h2>Fiyat:{getPost ? getPost.price : "Bilinmiyor"}</h2>
            <h2>Açıklama:{getPost ? getPost.descriptions : null}</h2>
          </Grid>
          <Grid item md={5} xs={12} pt={16} sx={{ marginTop: "100px" }}>
            <div>
              <iframe style={{ width: "100%", height: "350px", border: "0 !important" }} frameBorder={0}
                src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=1%20Grafton%20Street,%20Dublin,%20Ireland+(My%20Business%20Name)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed">
                <a href="https://www.gps.ie/wearable-gps/">wearable gps</a></iframe>
            </div>
          </Grid>
        </Grid>
      }
      {
        chatPanel
          ?
          <Grid container spacing={2} className={'chat-panel-container'} >
            <Grid item xs={12} className="chat-panel-header panel-row">
              Teklifiniz için mesaj yazın
              <Close sx={{ float: "right", cursor: "pointer" }} onClick={() => setChatPanel(false)} />
            </Grid>
            <Container maxWidth="lg" sx={{ overflow: "auto", maxHeight: 500 }}>
              <Grid item xs={12} className="chat-panel-body panel-row" >

                {
                  messages && messages.map((item, i) => <ChatMessage key={i} {...item} />)
                }

              </Grid>
            </Container>
            <Grid item xs={12} className="chat-panel-footer panel-row">
              <Grid container style={{ padding: "10px" }}>
                <Grid item xs={10}>
                  <TextField id="outlined-basic-email" label="Type Something" fullWidth value={text}
                    onChange={(e) => setText(e.target.value)} />
                </Grid>
                <Grid item xs={2} align="right">
                  <Fab color="primary" aria-label="add" onClick={() => messageSend()}>
                    <Send />
                  </Fab>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          : null

      }

    </Container>

  )

}

export default PostDetails;