import React, { useState } from 'react';
import {useDispatch} from "react-redux"
import { setAdmin } from '../features/authSlice';
import {Outlet, useNavigate } from "react-router-dom";
import AdminAccount from '../component/adminComponents/AdminAccountRegister.js';
import AdminHomePage from '../component/adminComponents/AdminHomePage';
import Customers from "../component/adminComponents/Customers";
import Posts from "../component/adminComponents/Posts";
import Profil from "../component/adminComponents/profil/Profil";

import { styled, createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import MuiDrawer from '@mui/material/Drawer';
import Box from '@mui/material/Box';
import { Container } from '@mui/material';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import { Button } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import Badge from '@mui/material/Badge';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import NotificationsIcon from '@mui/icons-material/Notifications';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import PeopleIcon from '@mui/icons-material/People';
import ListItemButton from '@mui/material/ListItemButton';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import GroupIcon from '@mui/icons-material/Group';
import AccountCircleIcon from '@mui/icons-material/AccountBox';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import StarBorder from '@mui/icons-material/StarBorder';
import HomeIcon from '@mui/icons-material/Home';


const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        '& .MuiDrawer-paper': {
            position: 'relative',
            whiteSpace: 'nowrap',
            width: drawerWidth,
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen,
            }),
            boxSizing: 'border-box',
            ...(!open && {
                overflowX: 'hidden',
                transition: theme.transitions.create('width', {
                    easing: theme.transitions.easing.sharp,
                    duration: theme.transitions.duration.leavingScreen,
                }),
                width: theme.spacing(10),
                [theme.breakpoints.up('sm')]: {
                    width: theme.spacing(9),
                },
            }),
        },
    }),
);

const mdTheme = createTheme();

function Admin() {
    const [open, setOpen] = useState(true);
    const dispatch = useDispatch()
    const navigate = useNavigate();
    const [openItemMenu, setOpenItemMenu] =useState(false);

    const handleClick = () => {
        setOpenItemMenu(!openItemMenu);
    };
    const toggleDrawer = () => {
        setOpen(!open);
        setOpenItemMenu(false);
    };
    const signOut = async () => {
        dispatch(setAdmin(null));
        localStorage.removeItem("token");
        navigate("/");
    }

    return (
        <>
            <ThemeProvider theme={mdTheme}>
                <Box sx={{ display: 'flex' }}>
                    <CssBaseline />
                    <AppBar position="absolute" open={open} sx={{ backgroundColor: "olive" }}>
                        <Container maxWidth="xxl">
                            <Toolbar
                                sx={{
                                    pr: '24px', // keep right padding when drawer closed
                                }}
                            >
                                <IconButton
                                    edge="start"
                                    color="inherit"
                                    aria-label="open drawer"
                                    onClick={toggleDrawer}
                                    sx={{
                                        marginRight: '36px',
                                        ...(open && { display: 'none' }),
                                    }}
                                >
                                    <MenuIcon />
                                </IconButton>
                                <Typography
                                    component="h1"
                                    variant="h6"
                                    color="inherit"
                                    noWrap
                                    sx={{ flexGrow: 1 }}
                                >
                                    ADMİN PANEL SAYFASI
                                </Typography>
                                <IconButton color="inherit">
                                    <Badge badgeContent={4} color="secondary">
                                        <NotificationsIcon />
                                    </Badge>
                                </IconButton>
                                {

                                    <Button
                                        sx={{ my: 2, color: 'white', display: 'block' }}
                                        onClick={signOut}
                                    >
                                        Signout

                                    </Button>
                                }
                            </Toolbar>
                        </Container>
                    </AppBar>

                    <Drawer variant="permanent" open={open} >
                        <Toolbar
                            sx={{
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'flex-end',
                                px: [1],
                            }}
                        >
                            <h3> Admin Kontrol Panel</h3>


                            <IconButton onClick={toggleDrawer}>
                                <ChevronLeftIcon />
                            </IconButton>
                        </Toolbar>

                        <Divider />
                        <List component="nav">
                        <ListItemButton onClick={() => navigate("admin")}>
                                <ListItemIcon>
                                    <StarBorder />
                                </ListItemIcon>
                                <ListItemText primary="Dashboard" />
                            </ListItemButton>
                            <Divider sx={{ my: 1 }} />
                            <ListItemButton onClick={handleClick}>
                                <ListItemIcon >
                                <StarBorder />
                                </ListItemIcon>
                                <ListItemText primary="Media Manage" />
                                 {openItemMenu ? <ExpandLess /> : <ExpandMore />}
                            </ListItemButton>
                            <Collapse in={openItemMenu} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    <ListItemButton sx={{ pl: 4 }} onClick={()=>navigate("homeMedia")}>
                                    <ListItemIcon>
                                <HomeIcon />
                                </ListItemIcon>   
                                        <ListItemText primary="Home" />
                                    </ListItemButton>
                                </List>
                            </Collapse>
                            <Divider sx={{ my: 1 }} />
                            <ListItemButton onClick={() => navigate("adminCuts")}>
                                <ListItemIcon>
                                    <GroupIcon />
                                </ListItemIcon>
                                <ListItemText primary="Customers" />
                            </ListItemButton>
                            <Divider sx={{ my: 1 }} />
                            <ListItemButton onClick={() => navigate("/adminPostCuts")}>
                                <ListItemIcon>
                                    <LocalShippingIcon />
                                </ListItemIcon>
                                <ListItemText primary="Posts" />
                            </ListItemButton>
                            <Divider sx={{ my: 1 }} />
                            <ListItemButton onClick={() => navigate("adminProfil")}>
                                <ListItemIcon >
                                    <PeopleIcon />
                                </ListItemIcon>
                                <ListItemText primary="Profil" />
                            </ListItemButton>
                            <Divider sx={{ my: 1 }} />
                            <ListItemButton onClick={() => navigate("adminRegister")}>
                                <ListItemIcon >
                                    <AccountCircleIcon />
                                </ListItemIcon>
                                <ListItemText primary="Account" />
                            </ListItemButton>
                        </List>
                    </Drawer>
                    <Box
                        component="main"
                        sx={{
                            backgroundColor: (theme) =>
                                theme.palette.mode === 'light'
                                    ? theme.palette.grey[100]
                                    : theme.palette.grey[900],
                            flexGrow: 1,
                            height: '100vh',
                            overflow: 'auto',
                            padding: 10
                        }}
                    >

                        <Outlet />

                    </Box>

                </Box>
            </ThemeProvider>

        </>
    );
}

export default Admin