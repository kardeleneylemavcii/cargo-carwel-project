import { Button, Divider } from "@mui/material";
import React, { useEffect, useState } from "react";
import axios from 'axios';
import ws from '../lib/WsConfig';
import waitForOpenConnection from "../lib/WsWaitForConn";
import { useNavigate, Outlet } from "react-router-dom";
import { Grid, Container, Box } from "@mui/material";
import TextField from '@mui/material/TextField';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import Stack from '@mui/material/Stack';
const ServerEndPoint = "http://localhost:8000/api/";

const Post = () => {
    const navigate = useNavigate();

    const [page, setPage] = useState(false);
    const [data, setData] = useState(null);
    const [carrData, setCarrData] = useState(null);
    const [customerListLoader, setCustomerListLoader] = useState(false);
    const [carrierListLoader, setCarrierListLoader] = useState(false);
    const [departure, setDeparture] = useState();
    const [arrival, setArrival] = useState();
    const [cityList, setCityList] = useState([]);
    const [departureDate, setDepartureDate] = useState();
    const [arrivalDate, setArrivalDate] = useState();



    const broadcast = (msg, room) => {
        console.log(msg, room, 'broadcast');
        ws.send(JSON.stringify({ room: room, msg: msg, broadcast }))
    }
    const join = (room, socket) => {
        console.log(room, 'join');
        ws.send(JSON.stringify({ join: room, ws: socket }));
    }
    let socket = ws;
    const group = 'post';

    useEffect(() => {

        if (socket.readyState !== socket.OPEN) {
            try {
                console.log("socket not open");
                waitForOpenConnection(socket).then(r => {
                    join(group, socket);
                })
            } catch (err) {
                console.error(err)
            }
        } else {
            console.log('else', socket);
            join(group, socket);
        }

        socket.onmessage = event => {
            if (event.data instanceof Blob) {
                const reader = new FileReader();

                reader.onload = () => {
                    console.log("Result: " + reader.result);
                };

                reader.readAsText(event.data);
            } else {
                console.log("Result: " + event.data);
            }
            // setCustomerListLoader(!customerListLoader);
            console.log(customerListLoader, "onMessage");
            /*      const getEventData = JSON.parse(evt.data);
                 const content = getEventData.message;
                 console.log(content);
                 if (!getEventData.room) {
                     return false;
                 }
                 if (content.userId === this.state.user.id) {
                   console.log(content);
                 } */
        };
    }, [customerListLoader])



    useEffect(() => {

        if (socket.readyState !== socket.OPEN) {
            try {
                console.log("socket not open");
                waitForOpenConnection(socket).then(r => {
                    join(group, socket);
                })
            } catch (err) {
                console.error(err)
            }
        } else {
            console.log('else', socket);
            join(group, socket);
        }

        socket.onmessage = event => {
            if (event.data instanceof Blob) {
                const reader = new FileReader();

                reader.onload = () => {
                    console.log("Result: " + reader.result);
                };

                reader.readAsText(event.data);
            } else {
                console.log("Result: " + event.data);
            }
            setCustomerListLoader(!customerListLoader);
            console.log(customerListLoader, "onMessage");
            /*      const getEventData = JSON.parse(evt.data);
                 const content = getEventData.message;
                 console.log(content);
                 if (!getEventData.room) {
                     return false;
                 }
                 if (content.userId === this.state.user.id) {
                   console.log(content);
                 } */
        };
    }, [customerListLoader])

    const [selectedIndex, setSelectedIndex] = React.useState(1);

    const handleListItemClick = (event, index) => {
        setSelectedIndex(index);
    };

    /*  useEffect(() => {
         console.log(customerListLoader, 'useEffect');
         axios.get(ServerEndPoint + "customer/custsList").then((res) => {
             //console.log(res);
             setData(res.data.customerList);
     
         }).catch(err => console.log(err))
     }, [customerListLoader]);
     
     useEffect(() => {
         axios.get(ServerEndPoint + "carrier/carrierList").then((res) => {
             // console.log(res);
             setCarrData(res.data.carrierList)
             //console.log(res)
         }).catch(err => console.log(err))
     }, [carrierListLoader]); */
    useEffect(() => {
        console.log("AddPostCarrier");
        axios.get(ServerEndPoint + "cities/list/223").then(res => {
            setCityList(res.data)
            console.log(res);
        }).catch(err =>
            console.log(err))
    }, []);

    const filterButton = ()=>{
        
    }

    return (

        <Container component="main" maxWidth="xxl" sx={{ padding: "0px",}} >
            <Grid container
                direction="row"
                justifyContent="flex-start"
                alignItems="flex-start"
            >
                <Grid item md={2} xs={2} sx={{ mt: 4, }}>
                    <Box
                        sx={{
                            border:3,
                            width: 300,
                            height: 700,
                            bgcolor: "#b0a2a2",
                            mt: 8,
                        }}
                    >
                        <Grid>
                            <select
                                className={"my-select"}
                                onChange={(e) => setDeparture([].slice.call(e.target.selectedOptions).map(item => {
                                    return { _id: item.value, name: item.text };
                                }))}
                            >
                                <option value="">Kalkış Noktası Seçiniz</option>
                                {cityList && cityList.map((city, i) => (
                                    <option key={i} value={city.id}>{city.name}</option>
                                ))}
                            </select>
                        </Grid>
                        <Grid sx={{ mt: 5 }}>
                            <select
                                className={"my-select"}
                                onChange={(e) => setArrival([].slice.call(e.target.selectedOptions).map(item => {
                                    return { _id: item.value, name: item.text };
                                }))}
                            >
                                <option value="">Varış Noktası Seçiniz</option>
                                {cityList && cityList.map((city, i) => (
                                    <option key={i} value={city.id}>{city.name}</option>
                                ))}
                            </select>
                        </Grid>
                        <Grid sx={{ mt: 5 }} >
                            <LocalizationProvider dateAdapter={AdapterDateFns}>
                                <Stack spacing={3}>

                                    <DatePicker
                                        renderInput={(params) => <TextField {...params} />}
                                        label="YÜKÜN TESLİM ALINACAĞI TARİH"
                                        value={departureDate}
                                        onChange={(newValue) => {
                                            setDepartureDate(newValue)
                                        }}
                                    />

                                    <DatePicker
                                        renderInput={(params) => <TextField {...params} />}
                                        label="YÜKÜN TESLİM EDİLECEĞİ TARİH"
                                        value={arrivalDate}
                                        onChange={(newValue) => {
                                            setArrivalDate(newValue);
                                        }}
                                    />
                                </Stack>
                            </LocalizationProvider>
                            <Grid item sx={{ display: "flex", flex: "row", justifyContent: "center" }}>
                            <Button variant="contained" sx={{ backgroundColor: "rgb(102 102 3)", mx: 2 }} onClick={() => navigate("/carrPost")} >
                                ARA
                            </Button>
                        </Grid>
                        </Grid>
                    </Box>
                   {/* <Button variant="contained" onClick={useContext} /> */}
                 </Grid>

                <Grid item md={10} xs={12}>
                    <Grid item sx={{ marginTop: 2 }} >
                        <Grid item sx={{ display: "flex", flex: "row", justifyContent: "center" }}>
                            <Button variant="contained" sx={{ backgroundColor: "rgb(26 66 57)", mx: 2 }} onClick={() => navigate("/carrPost")} >
                                Yük Alan
                            </Button>
                            <Button variant="contained" sx={{ backgroundColor: "#244769" }} onClick={() => navigate("/custPost")}>
                                Yük Veren
                            </Button>
                            <Button variant="contained" sx={{ backgroundColor: "rgb(102 102 3)", mx: 2 }} onClick={() => navigate("/post")}>
                                Tüm İlanlar
                            </Button>
                        </Grid>
                        <Divider sx={{ my: 1 }} />
                        <Grid item md={12} xs={12}>
                            <Outlet context={{ departure, arrival }} />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>






        </Container>


    )
}

export default Post;


