import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "../src/container/Home";
import About from "./container/About";
import MainLayout from "./container/MainLayout";
import NoPage from "./container/NoPage";
import SignIn from "./container/SignIn";
import ProfileSideBar from "./container/ProfileSideBar";
import Post from "./container/Post";
import PostCarrierPages from "./component/postPages/PostCarrierPages";
import PostCustomerPages from "./component/postPages/PostCustomerPages";
import PostAll from "./component/postPages/PostAll";
import PostDetails from "./container/PostDetails";
import UserRouter from "./UserRouter";
import AuthRouter from "./AuthRouter";
import AddPostPage from "./container/AddPostPage";
import Admin from "./container/Admin";
import AdminSignIn from "./container/AdminSignIn";
import VerifyEmail from "./component/account/VerifyEmail";
import Posts from "./component/adminComponents/Posts";
import AdminAccountRegister from "./component/adminComponents/AdminAccountRegister.js";
import Profil from "./component/adminComponents/profil/Profil";
import Customers from "./component/adminComponents/Customers";
import AdminHomePage from "./component/adminComponents/AdminHomePage";
import PostCarrier from "./component/adminComponents/PostCarrier";
import PostCustomer from "./component/adminComponents/PostCustomer";
import Profile from "./component/profilUser/Profile";
import HomeMedia from "./component/adminComponents/HomeMedia/HomeMedia";
import ForgotPassword from "./container/ForgotPassword";
import PasswordReset from "./container/PasswordReset";
import MyPosts from "./component/profilUser/MyPosts";
import MyFavorite from "./component/profilUser/MyFavorite";
import VerifiedEmail from "./container/VerifyAccount";
import Verification from "./container/VerifyAccountMessage";

function App() {
  const user = localStorage.getItem("token");

  const ROLES = {
    User: "user",
    Admin: "admin"
  }
  return (

    <BrowserRouter>
      <Routes>

        <Route element={<MainLayout />}>
          <Route path="/" index element={<Home />} />
          <Route path="about" element={<About />} />
          <Route path="verifyEmail" element={<VerifyEmail />} />
          <Route path="login" element={<SignIn />} />
          <Route path="forgotPassword" element={<ForgotPassword />} />
          <Route path="/passwordReset/:id/:token" element={<PasswordReset />} />
          <Route path="verifiedEmail" element={<VerifiedEmail />} />
          <Route path="/verified/:id/:token" element={<Verification />} />
          <Route element={<UserRouter allowedRoles={ROLES.User} allowedRolesAdmin={ROLES.Admin} />}>
            <Route path="addPost" element={<AddPostPage />} />
            <Route path="postDetails/:_type/:_id" element={<PostDetails />} />

          </Route>
          <Route element={<UserRouter allowedRoles={ROLES.User} />}>
            <Route element={<Post />}>
              <Route path="/carrPost" element={<PostCarrierPages />} />
              <Route path="/custPost" element={< PostCustomerPages />} />
              <Route path="/post" element={<PostAll />} />
            </Route>
          </Route>
        </Route>

        <Route element={<AuthRouter allowedRoles={ROLES.Admin} />}>
          <Route element={<Admin />}>
            <Route path="admin" element={<AdminHomePage />} />
            <Route path="homeMedia" element={<HomeMedia />} />
            <Route path="adminRegister" element={<AdminAccountRegister />} />
            <Route path="adminProfil" element={<Profil />} />
            <Route path="adminCuts" element={<Customers />} />
            <Route element={<Posts />} >
              <Route path="adminPostCuts" element={<PostCarrier />} />
              <Route path="adminPostCarrier" element={<PostCustomer />} />
            </Route>
          </Route>
        </Route>
        <Route path="adminSignIn" element={<AdminSignIn />} />
        <Route path="*" element={<NoPage />}
        />
        <Route element={<UserRouter allowedRoles={ROLES.User} allowedRolesAdmin={ROLES.Admin} />}>
          <Route element={<ProfileSideBar />} >
            <Route path="profil" element={<Profile />} />
            <Route path="myPosts" element={<MyPosts />} />
            <Route path="myFavorite" element={<MyFavorite />} />
          </Route>
        </Route>
      </Routes >
    </BrowserRouter >
  );
}

export default App;