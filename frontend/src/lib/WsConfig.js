let URL='ws://localhost:3030';
/* if (process.env.NODE_ENV === 'development') {
    URL = 'ws://localhost:3030';
} else {
    URL = 'wss://qooq.cloud:8443';
} */

const ws = new WebSocket(URL);
ws.onopen = function () {
    console.log("ws opening");
};
ws.onclose = function (e) {
    console.log('ws closed');
};

ws.onerror = function (err) {
    console.error('Socket encountered error: ', err.message, 'Closing socket');
    ws.close();
};

export default ws;