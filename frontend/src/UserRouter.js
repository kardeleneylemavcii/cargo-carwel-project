import { useLocation, Navigate, Outlet } from "react-router-dom";
import { useSelector } from "react-redux"

const UserRouter = ({ allowedRoles,allowedRolesAdmin}) => {
    const userData = useSelector((state) => state.login.data);
    const adminData = useSelector((state) => state.admin.data);
    const location = useLocation();
  
  
    return (
        (userData?.role !== allowedRoles || adminData?.role !== allowedRolesAdmin) ?
            <Outlet /> : <Navigate to="/login" state={{ from: location }} replace />
        
            
    );
}

export default UserRouter;