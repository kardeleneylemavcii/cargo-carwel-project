import { useLocation, Navigate, Outlet } from "react-router-dom";
import { useSelector } from "react-redux"

const AuthRouter = ({ allowedRoles }) => {
 
    const adminData = useSelector((state) => state.admin.data);
    const location = useLocation(); 
 
    return (
        (adminData?.role === allowedRoles) ?
            <Outlet /> : <Navigate to="/adminSignIn" state={{ from: location }} replace />
        
            
    );
}

export default AuthRouter;
